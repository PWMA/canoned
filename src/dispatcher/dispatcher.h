/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include "../dispatcherbase.h"

class Dispatcher : public DispatcherBase {
public:
	Dispatcher(std::shared_ptr<Channel<DispatchingMessage>> input,
			std::shared_ptr<Channel<NotificationMessage>> notifier,
			std::shared_ptr<Channel<StoreMessage>> dblogger);
	~Dispatcher();

	DispatcherBase::Result handle(std::shared_ptr<DispatchingAck> mesg) noexcept;
	DispatcherBase::Result handle(std::shared_ptr<DispatchingAddConfig> mesg) noexcept;
	DispatcherBase::Result handle(std::shared_ptr<DispatchingRemoveConfig> mesg) noexcept;
	DispatcherBase::Result handle(std::shared_ptr<DispatchingUpdate> mesg) noexcept;

	time_t prepare() noexcept;
	void timeout() noexcept;
#ifdef TEST
public:
#else
private:
#endif
	std::shared_ptr<Channel<DispatchingMessage>> input;

	class impl;
	std::unique_ptr<impl> pimpl;
};
