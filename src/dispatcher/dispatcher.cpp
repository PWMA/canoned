/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include "notification.h"
#include "dispatcher.h"

using namespace std;

struct Dispatcher::impl {
	multimap<time_t /* send_time */, 
		shared_ptr<Notification>> active_by_times;
	unordered_map<string /* label */, 
		shared_ptr<Notification>> all_by_labels;
	Dispatcher *disp;

	multimap<time_t, shared_ptr<Notification>>::iterator 
		find_active_alarm(time_t send_time_, const string &label_) noexcept;
	bool remove_active_alarm(time_t send_time_, const string &label_) noexcept;

	impl(Dispatcher *disp_);

	void print_structures() noexcept;

	void forward(shared_ptr<Notification> sptr) noexcept;
};
	
multimap<time_t, shared_ptr<Notification>>::iterator 
		Dispatcher::impl::find_active_alarm(time_t send_time_, 
				const string &label_) noexcept {
	auto range_it = active_by_times.equal_range(send_time_);
	for (auto active_it = range_it.first; active_it != range_it.second; 
			++active_it) {
		if (active_it->second->get_label() == label_)
			return active_it;
	}
	return active_by_times.end();
}

bool Dispatcher::impl::remove_active_alarm(time_t send_time_, 
		const string &label_) noexcept {
	bool ret = false;
	auto it = find_active_alarm(send_time_, label_);
	if (it != active_by_times.end()) {
		active_by_times.erase(it);
		ret = true;
	} 
	return ret;
}

Dispatcher::impl::impl(Dispatcher *disp_) : disp(disp_) {}

Dispatcher::Dispatcher(shared_ptr<Channel<DispatchingMessage>> input_, 
		shared_ptr<Channel<NotificationMessage>> notifier_,
		shared_ptr<Channel<StoreMessage>> dblogger_)
	: DispatcherBase(input_, notifier_, dblogger_)
	  , pimpl{make_unique<impl>(this)} {}

Dispatcher::~Dispatcher() {}

void Dispatcher::impl::print_structures() noexcept {
#ifndef NDEBUG
	stringstream log;
	log << __PRETTY_FUNCTION__;
	for (auto a : active_by_times) {
		log << " " << a.first 
			<< " " << *a.second.get() << endl;
	}
	for (auto a : all_by_labels) {
		log << *a.second.get() << endl;
	}
	clog << log.str();
#endif
}

void Dispatcher::impl::forward(shared_ptr<Notification> sptr) noexcept {
	vector<pair<string, string>> tokens = sptr->get_acked_tokens();
	if (tokens.size()) {
		/* Alarm fired so pass to notifier for deliver to users */
#ifndef NDEBUG
		stringstream log;
		log << __PRETTY_FUNCTION__ << " forward " 
			<< sptr->get_label() << " to";
		for (auto token : tokens) {
			log << " " << token.first
				<< " " << token.second;
		}
		clog << log.str() << endl;
#endif
		Result result;
		result.nmesg = make_shared<NotificationUserAlarm>
				(sptr->get_label(), sptr->get_time(),
				 tokens, sptr->get_note());
		disp->pass(result);
	}
	remove_active_alarm(sptr->get_send_time(), sptr->get_label());
	sptr->reset_acks();
	sptr->update_time(0);

	print_structures();
}

DispatcherBase::Result Dispatcher::handle(shared_ptr<DispatchingAck> mesg) noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " ack "  << mesg->label 
		<< " by " << mesg->token << endl;
#endif

	DispatcherBase::Result result;

	auto it = pimpl->all_by_labels.find(mesg->label);
	if (it != pimpl->all_by_labels.end()) {
		it->second->ack(mesg->token);

		result.smesg = make_shared<StoreAlarmAck>
			(mesg->label, mesg->token);
	
		pimpl->print_structures();
	}


	return result;
}

DispatcherBase::Result Dispatcher::handle(shared_ptr<DispatchingAddConfig> mesg) 
		noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " add "  << mesg->label 
		<< " for " << mesg->token << " with " 
		<< mesg->delay << " and ack " << mesg->ack << endl;
#endif

	DispatcherBase::Result result;

	auto it = pimpl->all_by_labels.find(mesg->label);
	if (it != pimpl->all_by_labels.end()) {
		/* Add token if notification object already exist */
		it->second->add_token(mesg->token, mesg->username, mesg->ack);
	} else {
		/* Create notification object (first time) */
		auto sptr = make_shared<Notification>(mesg->label, mesg->note,
				mesg->token, mesg->username, mesg->delay, mesg->ack);
		pimpl->all_by_labels.emplace(mesg->label, sptr);
	}

	if (! mesg->firstconf) {
		result.smesg = make_shared<StoreAlarmAdd>
			(mesg->label, mesg->token);
	}

	pimpl->print_structures();

	return result;
}

DispatcherBase::Result Dispatcher::handle(shared_ptr<DispatchingRemoveConfig> mesg) 
		noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " remove "  << mesg->label 
		<< " for " << mesg->token << endl;
#endif

	DispatcherBase::Result result;

	/* Find the relevant formula and remove token */
	auto it = pimpl->all_by_labels.find(mesg->label);
	if (it != pimpl->all_by_labels.end()) {
		it->second->remove_token(mesg->token);

		/* if tokens count reach zero remove notification object */
		if (! it->second->tokens_count()) {
			pimpl->remove_active_alarm(
					it->second->get_send_time(), 
					mesg->label);
			pimpl->all_by_labels.erase(it);
		}
	} else {
		result.nmesg = make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Logic error");
	}

	result.smesg = make_shared<StoreAlarmRemove>
		(mesg->label, mesg->token);

	pimpl->print_structures();

	return result;
}

DispatcherBase::Result Dispatcher::handle(shared_ptr<DispatchingUpdate> mesg) 
		noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " update "  << mesg->label 
		<< " " << mesg->result << endl;
#endif

	DispatcherBase::Result result;

	/* Find the relevant formula and its deadline 
	 * so use this to check if it is active */
	auto it = pimpl->all_by_labels.find(mesg->label);
	if (it != pimpl->all_by_labels.end()) {
		if (mesg->result) {
			if (! it->second->get_time()) {
				it->second->update_time(mesg->time);

				/* Activate it */
				pimpl->active_by_times.emplace(
						it->second->get_send_time(),
						it->second);
			}
		} else {
			if (it->second->get_time()) {
				/* Remove alarm */
				pimpl->remove_active_alarm(
						it->second->get_send_time(),
						mesg->label);
				it->second->update_time(0);
			}
		}
	} else {
		result.nmesg = make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Logic error");
	}
		
	pimpl->print_structures();

	return result;
}

time_t Dispatcher::prepare() noexcept {
	if (pimpl->active_by_times.size()) {
		return pimpl->active_by_times.begin()->first;
	}
	return 0xFFFFFFFF;
}

void Dispatcher::timeout() noexcept {
	pimpl->forward(pimpl->active_by_times.begin()->second);
}
