/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <vector>
#include <utility>
#include <unordered_map>

class Notification {
public:	
	Notification(const std::string &label_, const std::string &note_,
			const std::string &token_, const std::string &username_,
			const long delay_, const bool ack_);
	~Notification();

	const std::string& get_label() noexcept;
	const std::string& get_note() noexcept;
	time_t get_time() noexcept;
	time_t get_send_time() noexcept;

	bool add_token(const std::string &token_,
			const std::string &username_, const bool ack_) noexcept;
	bool remove_token(const std::string &token_) noexcept;
	size_t tokens_count() noexcept;


	void ack(const std::string &token_) noexcept;
	std::vector<std::pair<std::string /* token */ , std::string /* username */ >>
		get_acked_tokens() noexcept;
	void reset_acks() noexcept;

	void update_time(const time_t time) noexcept;
private:
	const std::string label, note;
	const long delay;

	std::unordered_map<std::string /* token */, 
		std::pair<bool /* ack */, std::string /* username */>> acks;
	time_t time;

	friend std::ostream& operator<<(std::ostream& dest, const Notification &value);
};

std::ostream& operator<<(std::ostream& dest, const Notification &value);
