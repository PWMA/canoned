/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sstream>
#include "notification.h"

using namespace std;

Notification::Notification(const string &label_, const string &note_,
		const string &token_, const string &username_,
		const long delay_, const bool ack_)
	: label(label_), note(note_), delay(delay_), time(0) {
			acks.emplace(token_, make_pair(ack_, username_));
}
	
Notification::~Notification() {}

const string& Notification::get_label() noexcept {
	return label;
}

const string& Notification::get_note() noexcept {
	return note;
}

time_t Notification::get_time() noexcept {
	return time;
}

time_t Notification::get_send_time() noexcept {
	return time + delay;
}

bool Notification::add_token(const string &token_,
		const string &username_, const bool ack_) noexcept {
	auto it = acks.emplace(token_, make_pair(ack_, username_));
	return it.second;
}

bool Notification::remove_token(const string &token_) noexcept {
	return acks.erase(token_);
}

size_t Notification::tokens_count() noexcept {
	return acks.size();
}

void Notification::reset_acks() noexcept {
	for(auto it = acks.begin(); it != acks.end(); ++it) {
		it->second.first = false;
	}
}

void Notification::ack(const string &token_) noexcept {
	auto it = acks.find(token_);
	if (it != acks.end()) {
		it->second.first = true;
	} 
}

vector<pair<string, string>> Notification::get_acked_tokens() noexcept {
	vector<pair<string, string>> ret;
	for (auto a : acks) {
		if (a.second.first)
			ret.push_back(make_pair(a.first, a.second.second));
	}
	return ret;
}

void Notification::update_time(const time_t time_) noexcept {
	time = time_;
}

ostream& operator<<(ostream& dest, const Notification &value) {
	stringstream log;
	log << "Label: " << value.label
#if 0
		<< " Note: " << value.note;
#endif
		<< " Delay: " << value.delay;
	for (auto a : value.acks) {
		log << " Token: " << a.first 
			<< " Username: " << a.second.second
			<< " ACK: " << a.second.first;
	}

	dest << log.str();
		
	return dest;
}
