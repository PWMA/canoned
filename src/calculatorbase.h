/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <vector>
#include "channel.h"
#include "messages.h"

class CalculatorBase {
public:
	CalculatorBase(unsigned int id,
			std::shared_ptr<Channel<CalculatingMessage>> input,
			std::shared_ptr<Channel<DispatchingMessage>> dispatcher,
			std::shared_ptr<Channel<NotificationMessage>> notifier);
	virtual ~CalculatorBase();

	void loop() noexcept;
protected:
	struct Result {
		std::vector<std::shared_ptr<DispatchingMessage>> dmesgs;
		std::shared_ptr<NotificationMessage> nmesg;
	};

	virtual void setup() noexcept;
	virtual time_t prepare() noexcept;
	virtual void timeout() noexcept;
	virtual Result handle(std::shared_ptr<CalculatingAddConfig> mesg) noexcept = 0;
	virtual Result handle(std::shared_ptr<CalculatingRemoveConfig> mesg) noexcept = 0;
	virtual Result handle(std::shared_ptr<CalculatingDataMessage> mesg) noexcept = 0;
	virtual void close() noexcept;
private:
	const unsigned int id;
	std::shared_ptr<Channel<CalculatingMessage>> input;
	std::shared_ptr<Channel<DispatchingMessage>> dispatcher;
	std::shared_ptr<Channel<NotificationMessage>> notifier;
	
	void handle(std::shared_ptr<CalculatingMessage> mesg) noexcept;
	void pass(Result result) noexcept;
};
