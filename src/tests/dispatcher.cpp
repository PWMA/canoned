/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#define TEST
#include "../dispatcher/notification.h"
#include "../dispatcher/dispatcher.h"

using namespace std;

struct Dispatcher::impl {
	multimap<time_t /* send_time */, 
		shared_ptr<Notification>> active_by_times;
	unordered_map<string /* label */, 
		shared_ptr<Notification>> all_by_labels;
	Dispatcher *disp;

	multimap<time_t, shared_ptr<Notification>>::iterator 
		find_active_alarm(time_t send_time_, const string &label_) noexcept;
	bool remove_active_alarm(time_t send_time_, const string &label_) noexcept;

	impl(Dispatcher *disp_);

	void print_structures() noexcept;

	void forward(shared_ptr<Notification> sptr) noexcept;
};

int main() {
#if 0
	dblogging_channel = make_shared<Channel<StoreMessage>>();
	notification_channel = make_shared<Channel<NotificationMessage>>();
	dispatching_channel = make_shared<Channel<DispatchingMessage>>();
	calculating_channels.push_back(make_shared<Channel<CalculatingMessage>>());
	subscribing_channel = make_shared<Channel<SubscriptionMessage>>();
	planning_channel = make_shared<Channel<PlanningMessage>>();
	receiving_channel = make_shared<Channel<Empty>>();

	unique_ptr<Dispatcher> dispatcher =
		make_unique<Dispatcher>(dispatching_channel, 
				notification_channel, dblogging_channel);

	vector<thread> threads;
	threads.emplace_back(bind(&Dispatcher::loop, dispatcher.get()));

	{
		clog << "TEST: Add a couple of items" << endl;
		dispatching_channel->write(make_shared<DispatchingAddConfig>
				("EtichettaA", "Note A", "AAAAAA", 2, "alessio.bogani", false));
		dispatching_channel->write(make_shared<DispatchingAddConfig>
				("EtichettaA", "Note A", "BBBBBB", 2, "alessio.bogani", false));
		
		dispatching_channel->write(make_shared<DispatchingAddConfig>
				("EtichettaB", "Note B", "BBBBBB", 3, "alessio.bogani", false));
		this_thread::sleep_for(chrono::milliseconds(100));
		assert(dispatcher->pimpl->all_by_labels.size() == 2);

		auto it1 = dispatcher->pimpl->all_by_labels.find("EtichettaA");
		assert(it1->second->tokens_count() == 2);
		auto it2 = dispatcher->pimpl->all_by_labels.find("EtichettaB");
		assert(it2->second->tokens_count() == 1);
		clog << endl;
	}

	{
		clog << "TEST: A too fast changing false->true->false" << endl;
		time_t start = time(NULL);
		dispatching_channel->write(make_shared<DispatchingUpdate>
				("EtichettaA", start, true));
		this_thread::sleep_for(chrono::milliseconds(100));
		assert(dispatcher->pimpl->active_by_times.size() == 1);
		dispatching_channel->write(make_shared<DispatchingUpdate>
				("EtichettaA", start + 1, false));
		this_thread::sleep_for(chrono::milliseconds(100));
		assert(dispatcher->pimpl->active_by_times.size() == 0);
		assert(notification_channel->size() == 0);
		clog << endl;
	}

	{
		clog << "TEST: Remove an active alarm" << endl;
		dispatching_channel->write(make_shared<DispatchingUpdate>
				("EtichettaA", time(NULL), true));
		this_thread::sleep_for(chrono::milliseconds(100));
		assert(dispatcher->pimpl->active_by_times.size() == 1);

		dispatching_channel->write(make_shared<DispatchingRemoveConfig>
				("EtichettaA", "AAAAAA"));
		dispatching_channel->write(make_shared<DispatchingRemoveConfig>
				("EtichettaA", "BBBBBB"));

		this_thread::sleep_for(chrono::milliseconds(100));
		assert(dispatcher->pimpl->active_by_times.size() == 0);
		assert(dispatcher->pimpl->all_by_labels.size() == 1);
		assert(notification_channel->size() == 0);
		clog << endl;
		
		clog << "TEST: Re-add the just removed alarms" << endl;
		dispatching_channel->write(make_shared<DispatchingAddConfig>
				("EtichettaA", "Note A", "AAAAAA", 2, "alessio.bogani", false));
		dispatching_channel->write(make_shared<DispatchingAddConfig>
				("EtichettaA", "Note A", "BBBBBB", 2, "alessio.bogani", false));
		this_thread::sleep_for(chrono::milliseconds(100));
		assert(dispatcher->pimpl->all_by_labels.size() == 2);
		clog << endl;
	}

	{
		clog << "TEST: Simulate updates" << endl;
		time_t start = time(NULL);
		dispatching_channel->write(make_shared<DispatchingUpdate>
				("EtichettaA", start, true));
		dispatching_channel->write(make_shared<DispatchingUpdate>
				("EtichettaB", start, true));
		this_thread::sleep_for(chrono::milliseconds(100));
		assert(dispatcher->pimpl->active_by_times.size() == 2);
		assert(notification_channel->size() == 0);
		
		vector<shared_ptr<NotificationMessage>> smesgs;
		notification_channel->read(smesgs, time(NULL) + 4);
		assert(smesgs.size() == 1);

		auto smesg = dynamic_pointer_cast<NotificationUserAlarm>(smesgs[0]);
		assert(smesg->label == "EtichettaA");
		assert(smesg->body == "Note A");
		set<string> expected{"AAAAAA", "BBBBBB"};
		assert(smesg->tokens == expected);
		assert(dispatcher->pimpl->active_by_times.size() == 1);
		assert(notification_channel->size() == 0);

		smesgs.clear();
		notification_channel->read(smesgs, time(NULL) + 3);
		assert(smesgs.size() == 1);

		smesg = dynamic_pointer_cast<NotificationUserAlarm>(smesgs[0]);
		assert(smesg->label == "EtichettaB");
		assert(smesg->body == "Note B");
		assert(smesg->tokens == set<string>{"BBBBBB"});
		assert(dispatcher->pimpl->active_by_times.size() == 0);
		assert(notification_channel->size() == 0);
		clog << endl;
	}

	{
		clog << "TEST: Simulate the same updates but already acked" << endl;
		time_t start = time(NULL);
		dispatching_channel->write(make_shared<DispatchingUpdate>
				("EtichettaA", start, true));
		dispatching_channel->write(make_shared<DispatchingUpdate>
				("EtichettaB", start, true));
		this_thread::sleep_for(chrono::seconds(4));
		assert(notification_channel->size() == 0);
		clog << endl;

		clog << "TEST: Simulate acks and so send updates again" << endl;
		start = time(NULL);
		dispatching_channel->write(make_shared<DispatchingAck>
				("EtichettaA", "AAAAAA"));
		dispatching_channel->write(make_shared<DispatchingAck>
				("EtichettaA", "BBBBBB"));
		dispatching_channel->write(make_shared<DispatchingAck>
				("EtichettaB", "BBBBBB"));

		dispatching_channel->write(make_shared<DispatchingUpdate>
				("EtichettaA", start, true));
		dispatching_channel->write(make_shared<DispatchingUpdate>
				("EtichettaB", start, true));
		this_thread::sleep_for(chrono::milliseconds(100));
		assert(dispatcher->pimpl->active_by_times.size() == 2);
		assert(notification_channel->size() == 0);
		this_thread::sleep_for(chrono::seconds(4));
		assert(dispatcher->pimpl->active_by_times.size() == 0);

		vector<shared_ptr<NotificationMessage>> smesgs;
		notification_channel->read(smesgs, 0);
		assert(smesgs.size() == 2);
		auto smesg0 = dynamic_pointer_cast<NotificationUserAlarm>(smesgs[0]);
		assert(smesg0->label == "EtichettaA");
		assert(smesg0->body == "Note A");
		set<string> expected{"AAAAAA", "BBBBBB"};
		assert(smesg0->tokens == expected);
		auto smesg1 = dynamic_pointer_cast<NotificationUserAlarm>(smesgs[1]);
		assert(smesg1->label == "EtichettaB");
		assert(smesg1->body == "Note B");
		assert(smesg1->tokens == set<string>{"BBBBBB"});
		clog << endl;
 	}

	{
		clog << "TEST: Remove all items" << endl;
		dispatching_channel->write(make_shared<DispatchingRemoveConfig>
				("EtichettaA", "AAAAAA"));
		dispatching_channel->write(make_shared<DispatchingRemoveConfig>
				("EtichettaA", "BBBBBB"));
		dispatching_channel->write(make_shared<DispatchingRemoveConfig>
				("EtichettaB", "BBBBBB"));
	}

	this_thread::sleep_for(chrono::milliseconds(100));

	assert(dispatcher->pimpl->all_by_labels.size() == 0);
	assert(dispatcher->pimpl->active_by_times.size() == 0);
	assert(dispatching_channel->size() == 0);
	assert(notification_channel->size() == 0);

	dispatching_channel->close();
	threads[0].join();
#endif
}
