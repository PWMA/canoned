/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#define TEST
#include "../calculator/formula.h"
#include "../calculator/calculator.h"

using namespace std;

struct Calculator::impl {
	unordered_multimap<string /* attribute */, 
			shared_ptr<Formula>> operands;

	void print_structures() noexcept;

	template<typename T> CalculatorBase::Result calc(shared_ptr<T> mesg) noexcept;
};

int main() {
	notification_channel = make_shared<Channel<NotificationMessage>>();
	dispatching_channel = make_shared<Channel<DispatchingMessage>>();
	subscribing_channel = make_shared<Channel<SubscriptionMessage>>();
	planning_channel = make_shared<Channel<PlanningMessage>>();
	receiving_channel = make_shared<Channel<Empty>>();

	vector<unique_ptr<Calculator>> calculators;
	vector<thread> threads;
	for (unsigned int i=0; i<2; ++i) {
		calculating_channels.emplace_back(make_shared<Channel<CalculatingMessage>>());
		calculators.emplace_back(make_unique<Calculator>(i, calculating_channels[i],
					dispatching_channel, notification_channel));
		threads.emplace_back(bind(&Calculator::loop, calculators[i].get()));
	}	

	{
		clog << "TEST: Add few formulas (single attribute, multi-attribute"
			" and invalid)" << endl;
		calculating_channels[0]->write(make_shared<CalculatingAddConfig>
				("EtichettaOne",
				 "tango://ken:20000/ken/tango/test/long_scalar > 1",
				 set<string>{"tango://ken:20000/ken/tango/test/long_scalar"}));
		calculating_channels[1]->write(make_shared<CalculatingAddConfig>
				("EtichettaTwo",
				 "(tango://ken:20000/ken/tango/test/long_scalar + "
				 "tango://ken:20000/ken/tango/test/ulong_scalar) > 10",
				 set<string>{
				 "tango://ken:20000/ken/tango/test/long_scalar",
				 "tango://ken:20000/ken/tango/test/ulong_scalar"}));

		calculating_channels[0]->write(make_shared<CalculatingAddConfig>
				("EtichettaInvalid",
				 "tango://ken:20000/ken/tango/test/long_scalar + "
				 "tango://ken:20000/ken/tango/test/ulong_scalar",
				 set<string>{
				 "tango://ken:20000/ken/tango/test/long_scalar",
				 "tango://ken:20000/ken/tango/test/ulong_scalar"}));

		this_thread::sleep_for(chrono::milliseconds(100));
		assert(calculating_channels[0]->size() == 0);
		assert(calculators[0]->pimpl->operands.size() == 3);
		assert(calculating_channels[1]->size() == 0);
		assert(calculators[1]->pimpl->operands.size() == 2);
		clog << endl;
	}

	{
		clog << "TEST: Send a value" << endl;

		auto mesg = make_shared<IntData>
			("tango://ken:20000/ken/tango/test/long_scalar", time(NULL), 10);
		for (unsigned int i=0; i<2; ++i) {
			calculating_channels[i]->write(mesg);
		}

		vector<shared_ptr<DispatchingMessage>> dmesgs;
		dispatching_channel->read(dmesgs, time(NULL) + 2);
		assert(dmesgs.size() == 1);
		auto dmesg = dynamic_pointer_cast<DispatchingUpdate>(dmesgs[0]);
		assert(dmesg->label == "EtichettaOne");
		assert(dmesg->result == true);

		clog << endl;
	}

	{
		clog << "TEST: Send another value" << endl;

		auto mesg = make_shared<IntData>
			("tango://ken:20000/ken/tango/test/ulong_scalar", time(NULL), 0);

		for (unsigned int i=0; i<2; ++i) {
			calculating_channels[i]->write(mesg);
		}
		
		vector<shared_ptr<DispatchingMessage>> dmesgs;
		dispatching_channel->read(dmesgs, time(NULL) + 1);
		assert(dmesgs.size() == 1);
		auto dmesg0 = dynamic_pointer_cast<DispatchingUpdate>(dmesgs[0]);
		assert(dmesg0->label == "EtichettaTwo");
		assert(dmesg0->result == false);

		clog << endl;
	}

	{
		clog << "TEST: Send another value (again)" << endl;

		auto mesg = make_shared<IntData>
			("tango://ken:20000/ken/tango/test/long_scalar", time(NULL), 11);

		for (unsigned int i=0; i<2; ++i) {
			calculating_channels[i]->write(mesg);
		}
		
		this_thread::sleep_for(chrono::milliseconds(500));

		vector<shared_ptr<DispatchingMessage>> dmesgs;
		dispatching_channel->read(dmesgs, 0);
		assert(dmesgs.size() == 2);
		auto dmesg0 = dynamic_pointer_cast<DispatchingUpdate>(dmesgs[0]);
		auto dmesg1 = dynamic_pointer_cast<DispatchingUpdate>(dmesgs[1]);

		if (dmesg0->label == "EtichettaOne") {
			assert(dmesg0->result == true);

			assert(dmesg1->label == "EtichettaTwo");
			assert(dmesg1->result == true);
		} else if (dmesg1->label == "EtichettaOne") {
			assert(dmesg1->result == true);
				
			assert(dmesg0->label == "EtichettaTwo");
			assert(dmesg0->result == true);
		} else { 
			assert(false);
		}

		clog << endl;
	}

	{
		clog << "TEST: Remove formulas" << endl;
		calculating_channels[0]->write(make_shared<CalculatingRemoveConfig>
				("EtichettaOne",
				 set<string>{"tango://ken:20000/ken/tango/test/long_scalar"}));
		
		calculating_channels[1]->write(make_shared<CalculatingRemoveConfig>
				("EtichettaTwo",
				 set<string>{
				 "tango://ken:20000/ken/tango/test/long_scalar",
				 "tango://ken:20000/ken/tango/test/ulong_scalar"}));
		
		calculating_channels[0]->write(make_shared<CalculatingRemoveConfig>
				("EtichettaInvalid",
				 set<string>{
				 "tango://ken:20000/ken/tango/test/long_scalar",
				 "tango://ken:20000/ken/tango/test/ulong_scalar"}));

		this_thread::sleep_for(chrono::milliseconds(100));
		assert(calculating_channels[0]->size() == 0);
		assert(calculators[0]->pimpl->operands.size() == 0);
		assert(calculating_channels[1]->size() == 0);
		assert(calculators[1]->pimpl->operands.size() == 0);
		clog << endl;
	}

	this_thread::sleep_for(chrono::milliseconds(100));

	for (unsigned int i=0; i<2; ++i) {
		assert(calculating_channels[i]->size() == 0);
		calculating_channels[i]->close();
		threads[i].join();
	}

	assert(dispatching_channel->size() == 0);
}
