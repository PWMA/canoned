/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include <libpq-fe.h>
#define TEST
#include "../receiver/receiver.h"

using namespace std;

struct Receiver::impl {
	const string conninfo;
	int pipefd[2];
	PGconn *conn;
	PGnotify *notify;
	thread receiving_thread;
	Receiver *recv;
	shared_ptr<Channel<NotificationMessage>> notifier;

	impl(Receiver *recv_, const string &conninfo_,
			shared_ptr<Channel<NotificationMessage>> notifier_);
	~impl();

	void forward_alarm(const string &mesg, const bool firstconf) noexcept;
	void forward_token(const string &mesg) noexcept;
	void receive() noexcept;
};

class TestableReceiver : public Receiver {
public:
	TestableReceiver(const string &conninfo, 
			shared_ptr<Channel<Empty>> input, 
			shared_ptr<Channel<PlanningMessage>> planner,
			shared_ptr<Channel<DispatchingMessage>> dispatcher,
			shared_ptr<Channel<NotificationMessage>> notifier,
			shared_ptr<Channel<StoreMessage>> dblogger)
		: Receiver(conninfo, input, planner, dispatcher,
				notifier, dblogger) {}
	~TestableReceiver() {}
	
	void setup() noexcept {}
};

int main() {
	dblogging_channel = make_shared<Channel<StoreMessage>>();
	notification_channel = make_shared<Channel<NotificationMessage>>();
	dispatching_channel = make_shared<Channel<DispatchingMessage>>();
	calculating_channels.push_back(make_shared<Channel<CalculatingMessage>>());
	subscribing_channel = make_shared<Channel<SubscriptionMessage>>();
	planning_channel = make_shared<Channel<PlanningMessage>>();
	receiving_channel = make_shared<Channel<Empty>>();

	unique_ptr<TestableReceiver> receiver =
		make_unique<TestableReceiver>("", receiving_channel, 
				planning_channel, dispatching_channel,
				notification_channel, dblogging_channel);
	
	string token = "AAAAAA";
	time_t wait_until = time(NULL) + 1;

	{
		vector<pair<string, string>> mesgs {
			{ "Completely empty string", "" },
			{ "Almost empty string", "{\"event\": \"\"}" },
			{ "Empty data object", "{\"event\": \"add\", \"data\":{}}" },
			{ "String with label only", "{\"event\": \"add\","
				"\"data\":{\"label\": \"Etichetta1\"}}" },
			{ "Missing formula", "{\"event\": \"add\","
				"\"data\":{\"label\": \"Etichetta1\", \"note\": \"Note1\","
				"\"token\": \"" + token +"\"}}" },
			{ "Unspecified formula", "{\"event\": \"add\", \"data\":{"
				"\"label\": \"Etichetta1\", \"note\": \"Note1\","
				"\"token\": \"" + token + "\", \"formula\": }}" },
			{ "Wrong event type and use uppercase letters for formula",
				"{\"event\": \"new\", \"data\":{"
				"\"label\": \"etichetta1\", \"note\": \"Note1\","
				"\"token\": \"" + token + "\", \"formula\": \"A + B\"}}" }};

		for (auto mesg : mesgs) {
			cerr << "TEST: " <<  mesg.first << endl;
			receiver->pimpl->forward_alarm(mesg.second, false);
			this_thread::sleep_for(chrono::milliseconds(100));
			assert(dispatching_channel->size() == 0 
					&& planning_channel->size() == 0);
			cerr << endl;
		}
	}

	{
		vector<pair<string, string>> mesgs {
	       		{ "Invalid character (LF)", "{\"event\": \"add\", \"data\":{"
				"\"label\": \"Etichetta1\", \"note\": \"Note1\", \"token\":"
				" \"" + token + "\", \"formula\": \"A +\n B\"}}" },
	       		{ "Perfectly fine (add)", "{\"event\": \"add\", \"data\":{"
				"\"label\": \"Etichetta1\", \"note\": \"Note1\", \"token\":"
				" \"" + token + "\", \"formula\": \"A + B\"}}" }};
		
		for (auto mesg : mesgs) {
			cerr << "TEST: " <<  mesg.first << endl;
			receiver->pimpl->forward_alarm(mesg.second, false);
	
			vector<shared_ptr<PlanningMessage>> pmesgs;
			planning_channel->read(pmesgs, wait_until);
			assert(pmesgs.size() == 1 && planning_channel->size() == 0
				&& pmesgs[0]->type == Message::SUBSCRIBE);

			shared_ptr<PlanningSubscribe> psub =
				dynamic_pointer_cast<PlanningSubscribe>(pmesgs[0]);
			assert(psub->label == "Etichetta1" && psub->token == token
					&& psub->note == "Note1" && psub->formula == "a + b"
					&& psub->delay == 0L);
			assert(dispatching_channel->size() == 0 
					&& planning_channel->size() == 0);
			cerr << endl;
		}
	}
	
	{
		vector<pair<string, string>> mesgs {
			{ "Perfectly fine (remove)",  "{\"event\": \"remove\", \"data\":{"
				"\"label\": \"Etichetta1\", \"token\":"	
				" \"" + token + "\"}}" }};
		
		for (auto mesg : mesgs) {
			cerr << "TEST: " <<  mesg.first << endl;
			receiver->pimpl->forward_alarm(mesg.second, false);
	
			vector<shared_ptr<PlanningMessage>> pmesgs;
			planning_channel->read(pmesgs, wait_until);
			assert(pmesgs.size() == 1 && planning_channel->size() == 0
				&& pmesgs[0]->type == Message::UNSUBSCRIBE);

			shared_ptr<PlanningUnsubscribe> punsub =
				dynamic_pointer_cast<PlanningUnsubscribe>(pmesgs[0]);
			assert(punsub->label == "Etichetta1" && punsub->token == token);
			assert(dispatching_channel->size() == 0 
					&& planning_channel->size() == 0);
			cerr << endl;
		}
	}
	
	{
		vector<pair<string, string>> mesgs {
			{ "Perfectly fine (ack)", "{\"event\": \"ack\", \"data\":{"
				"\"label\": \"Etichetta1\", \"token\":"	
				" \"" + token + "\"}}"}};
		
		for (auto mesg : mesgs) {
			cerr << "TEST: " <<  mesg.first << endl;
			receiver->pimpl->forward_alarm(mesg.second, false);

			vector<shared_ptr<DispatchingMessage>> pmesgs;
			dispatching_channel->read(pmesgs, wait_until);
			assert(pmesgs.size() == 1 && dispatching_channel->size() == 0
				&& pmesgs[0]->type == Message::ACK);

			shared_ptr<DispatchingAck> dack =
				dynamic_pointer_cast<DispatchingAck>(pmesgs[0]);
			assert(dack->label == "Etichetta1" && dack->token == token);
			assert(dispatching_channel->size() == 0 
					&& planning_channel->size() == 0);
			cerr << endl;
		}
	}
}
