/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#define TEST
#include "../planner/plan.h"
#include "../planner/planner.h"

using namespace std;

struct Planner::impl {
	/* Calculators' index (Round robin) */
	unsigned int calc_id;
	const unsigned int max_calc_id;
	unsigned int get_next_calc_id() noexcept;

	unordered_map<string /* label */, 
		shared_ptr<Plan>> plans;
	unordered_map<string /* attribute */, 
		long /* Reference counter */> attributes;

	impl(unsigned int max_calc_id_);
	~impl();

	void print_structures() noexcept;
};

int main() {
	notification_channel = make_shared<Channel<NotificationMessage>>();
	dispatching_channel = make_shared<Channel<DispatchingMessage>>();
	calculating_channels.push_back(make_shared<Channel<CalculatingMessage>>());
	calculating_channels.push_back(make_shared<Channel<CalculatingMessage>>());
	subscribing_channel = make_shared<Channel<SubscriptionMessage>>();
	planning_channel = make_shared<Channel<PlanningMessage>>();
	receiving_channel = make_shared<Channel<Empty>>();

	unique_ptr<Planner> planner =
		make_unique<Planner>(planning_channel, 
				subscribing_channel, calculating_channels,
				dispatching_channel, notification_channel);

	vector<thread> threads;
	threads.emplace_back(bind(&Planner::loop, planner.get()));

	string formulaA = "tango://ken:20000/ken/tango/test/long_scalar + "
		"tango://ken:20000/ken/tango/test/short_scalar";
	set<string> attributesA { "tango://ken:20000/ken/tango/test/long_scalar", 
		"tango://ken:20000/ken/tango/test/short_scalar" };

	string formulaB = "tango://ken:20000/ken/tango/test/short_scalar + "
		"tango://ken:20000/ken/tango/test/ulong_scalar";
	set<string> attributesB { "tango://ken:20000/ken/tango/test/short_scalar", 
		"tango://ken:20000/ken/tango/test/ulong_scalar" };

	time_t wait_until = time(NULL) + 2;

	{
		clog << "TEST: Add a new formula" << endl;
		planning_channel->write(make_shared<PlanningSubscribe>
				("EtichettaA", "AAAAAA", formulaA, "Note A",
				 10, "alessio.bogani", true, false));

		vector<shared_ptr<SubscriptionMessage>> smesgs;
		subscribing_channel->read(smesgs, wait_until);
		assert(smesgs.size() == 1);
		auto smesg = dynamic_pointer_cast<SubscribeAttributes>(smesgs[0]);
		assert(smesg->attributes == attributesA);

		vector<shared_ptr<CalculatingMessage>> cmesgs;
		calculating_channels[0]->read(cmesgs, wait_until);
		assert(cmesgs.size() == 1);
		auto cmesg = dynamic_pointer_cast<CalculatingAddConfig>(cmesgs[0]);
		assert(cmesg->label == "EtichettaA");
		assert(cmesg->formula == formulaA);
		assert(cmesg->attributes == attributesA);

		vector<shared_ptr<DispatchingMessage>> dmesgs;
		dispatching_channel->read(dmesgs, wait_until);
		assert(dmesgs.size() == 1);
		auto dmesg = dynamic_pointer_cast<DispatchingAddConfig>(dmesgs[0]);
		assert(dmesg->label == "EtichettaA");
		assert(dmesg->token == "AAAAAA");
		assert(dmesg->delay == 10);

		assert(planning_channel->size() == 0 && subscribing_channel->size() == 0
				&& calculating_channels[0]->size() == 0 
				&& calculating_channels[1]->size() == 0 
				&& dispatching_channel->size() == 0);
		clog << endl;
	}
	assert(planner->pimpl->plans.size() == 1);
	assert(planner->pimpl->attributes.size() == 2);
	assert(planner->pimpl->plans.begin()->second->tokens.size() == 1);

	{
		clog << "TEST: Add the same formula for same token (duplicated)" << endl;
		planning_channel->write(make_shared<PlanningSubscribe>
				("EtichettaA", "AAAAAA", formulaA, "Note A",
				 10, "alessio.bogani", true, false));
		this_thread::sleep_for(chrono::milliseconds(100));
		assert(planning_channel->size() == 0 && subscribing_channel->size() == 0
				&& calculating_channels[0]->size() == 0 
				&& calculating_channels[1]->size() == 0 
				&& dispatching_channel->size() == 0);
		clog << endl;
	}
	assert(planner->pimpl->plans.size() == 1);
	assert(planner->pimpl->attributes.size() == 2);
	assert(planner->pimpl->plans.begin()->second->tokens.size() == 1);

	{
		clog << "TEST: Add the same formula for a different token" << endl;
		planning_channel->write(make_shared<PlanningSubscribe>
				("EtichettaA", "BBBBBB", formulaB, "Note B",
				 10, "alessio.bogani", true, false));
		vector<shared_ptr<DispatchingMessage>> dmesgs;
		dispatching_channel->read(dmesgs, wait_until);
		assert(dmesgs.size() == 1);
		auto dmesg = dynamic_pointer_cast<DispatchingAddConfig>(dmesgs[0]);
		assert(dmesg->label == "EtichettaA");
		assert(dmesg->token == "BBBBBB");
		assert(dmesg->delay == 10);

		assert(planning_channel->size() == 0 && subscribing_channel->size() == 0
				&& calculating_channels[0]->size() == 0 
				&& calculating_channels[1]->size() == 0 
				&& dispatching_channel->size() == 0);
		clog << endl;
	}
	assert(planner->pimpl->plans.size() == 1);
	assert(planner->pimpl->attributes.size() == 2);
	assert(planner->pimpl->plans.begin()->second->tokens.size() == 2);

	{
		clog << "TEST: Add an other formula with a common attribute" << endl;
		planning_channel->write(make_shared<PlanningSubscribe>
				("EtichettaB", "BBBBBB", formulaB, "Note B",
				 10, "alessio.bogani", true, false));
		
		vector<shared_ptr<SubscriptionMessage>> smesgs;
		subscribing_channel->read(smesgs, wait_until);
		assert(smesgs.size() == 1);
		auto smesg = dynamic_pointer_cast<SubscribeAttributes>(smesgs[0]);
		assert(smesg->attributes == set<string>
				{"tango://ken:20000/ken/tango/test/ulong_scalar"});

		vector<shared_ptr<CalculatingMessage>> cmesgs;
		calculating_channels[1]->read(cmesgs, wait_until);
		assert(cmesgs.size() == 1);
		auto cmesg = dynamic_pointer_cast<CalculatingAddConfig>(cmesgs[0]);
		assert(cmesg->label == "EtichettaB");
		assert(cmesg->formula == formulaB);
		assert(cmesg->attributes == attributesB);

		vector<shared_ptr<DispatchingMessage>> dmesgs;
		dispatching_channel->read(dmesgs, wait_until);
		assert(dmesgs.size() == 1);
		auto dmesg = dynamic_pointer_cast<DispatchingAddConfig>(dmesgs[0]);
		assert(dmesg->label == "EtichettaB");
		assert(dmesg->token == "BBBBBB");
		assert(dmesg->delay == 10);

		assert(planning_channel->size() == 0 && subscribing_channel->size() == 0
				&& calculating_channels[0]->size() == 0 
				&& calculating_channels[1]->size() == 0 
				&& dispatching_channel->size() == 0);

		clog << endl;
	}
	assert(planner->pimpl->plans.size() == 2);
	assert(planner->pimpl->attributes.size() == 3);
	assert(planner->pimpl->plans.find("EtichettaA")->second->tokens.size() == 2);
	assert(planner->pimpl->plans.find("EtichettaB")->second->tokens.size() == 1);

	{
		clog << "TEST: Unsubscribe one token form a subscribed formula" << endl;
		planning_channel->write(make_shared<PlanningUnsubscribe>
				("EtichettaA", "BBBBBB"));
		vector<shared_ptr<DispatchingMessage>> dmesgs;
		dispatching_channel->read(dmesgs, wait_until);
		assert(dmesgs.size() == 1);
		auto dmesg = dynamic_pointer_cast<DispatchingRemoveConfig>(dmesgs[0]);
		assert(dmesg->label == "EtichettaA");
		assert(dmesg->token == "BBBBBB");

		assert(planning_channel->size() == 0 && subscribing_channel->size() == 0
				&& calculating_channels[0]->size() == 0 
				&& calculating_channels[1]->size() == 0 
				&& dispatching_channel->size() == 0);

		clog << endl;
	}
	assert(planner->pimpl->plans.size() == 2);
	assert(planner->pimpl->attributes.size() == 3);
	assert(planner->pimpl->plans.find("EtichettaA")->second->tokens.size() == 1);
	assert(planner->pimpl->plans.find("EtichettaB")->second->tokens.size() == 1);

	{
		clog << "TEST: Unsubscribe last token form a subscribed formula" << endl;
		planning_channel->write(make_shared<PlanningUnsubscribe>
				("EtichettaA", "AAAAAA"));
		
		vector<shared_ptr<SubscriptionMessage>> smesgs;
		subscribing_channel->read(smesgs, wait_until);
		assert(smesgs.size() == 1);
		auto smesg = dynamic_pointer_cast<UnsubscribeAttributes>(smesgs[0]);
		assert(smesg->attributes == set<string>{"tango://ken:20000/ken/tango/test/long_scalar"});

		vector<shared_ptr<CalculatingMessage>> cmesgs;
		calculating_channels[0]->read(cmesgs, wait_until);
		assert(cmesgs.size() == 1);
		auto cmesg = dynamic_pointer_cast<CalculatingRemoveConfig>(cmesgs[0]);
		assert(cmesg->label == "EtichettaA");
		assert(cmesg->attributes == attributesA);

		vector<shared_ptr<DispatchingMessage>> dmesgs;
		dispatching_channel->read(dmesgs, wait_until);
		assert(dmesgs.size() == 1);
		auto dmesg = dynamic_pointer_cast<DispatchingRemoveConfig>(dmesgs[0]);
		assert(dmesg->label == "EtichettaA");
		assert(dmesg->token == "AAAAAA");

		assert(planning_channel->size() == 0 && subscribing_channel->size() == 0
				&& calculating_channels[0]->size() == 0 
				&& calculating_channels[1]->size() == 0 
				&& dispatching_channel->size() == 0);

		clog << endl;
	}
	assert(planner->pimpl->plans.size() == 1);
	assert(planner->pimpl->attributes.size() == 2);
	assert(planner->pimpl->plans.find("EtichettaA") == planner->pimpl->plans.end());
	assert(planner->pimpl->plans.find("EtichettaB")->second->tokens.size() == 1);

	{
		clog << "TEST: Unsubscribe the last one" << endl;
		planning_channel->write(make_shared<PlanningUnsubscribe>
				("EtichettaB", "BBBBBB"));
		
		vector<shared_ptr<SubscriptionMessage>> smesgs;
		subscribing_channel->read(smesgs, wait_until);
		assert(smesgs.size() == 1);
		auto smesg = dynamic_pointer_cast<UnsubscribeAttributes>(smesgs[0]);
		assert(smesg->attributes == attributesB);

		vector<shared_ptr<CalculatingMessage>> cmesgs;
		calculating_channels[1]->read(cmesgs, wait_until);
		assert(cmesgs.size() == 1);
		auto cmesg = dynamic_pointer_cast<CalculatingRemoveConfig>(cmesgs[0]);
		assert(cmesg->label == "EtichettaB");
		assert(cmesg->attributes == attributesB);

		vector<shared_ptr<DispatchingMessage>> dmesgs;
		dispatching_channel->read(dmesgs, wait_until);
		assert(dmesgs.size() == 1);
		auto dmesg = dynamic_pointer_cast<DispatchingRemoveConfig>(dmesgs[0]);
		assert(dmesg->label == "EtichettaB");
		assert(dmesg->token == "BBBBBB");

		assert(planning_channel->size() == 0 && subscribing_channel->size() == 0
				&& calculating_channels[0]->size() == 0 
				&& calculating_channels[1]->size() == 0 
				&& dispatching_channel->size() == 0);

		clog << endl;
	}
	assert(planner->pimpl->plans.size() == 0);
	assert(planner->pimpl->attributes.size() == 0);
	assert(planner->pimpl->plans.find("EtichettaA") == planner->pimpl->plans.end());
	assert(planner->pimpl->plans.find("EtichettaB") == planner->pimpl->plans.end());

	planning_channel->close();
	threads[0].join();
}
