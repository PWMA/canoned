/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include <tango.h>
#define TEST
#include "../cache.h"
#include "../subscriber/subscription.h"
#include "../subscriber/subscriber.h"

using namespace std;

ostream& operator<<(ostream& dest, __int128_t value)
{
    ostream::sentry s( dest );
    if ( s ) {
        __uint128_t tmp = value < 0 ? -value : value;
        char buffer[ 128 ];
        char* d = end( buffer );
        do
        {
            -- d;
            *d = "0123456789"[ tmp % 10 ];
            tmp /= 10;
        } while ( tmp != 0 );
        if ( value < 0 ) {
            -- d;
            *d = '-';
        }
        int len = end( buffer ) - d;
        if ( dest.rdbuf()->sputn( d, len ) != len ) {
            dest.setstate(ios_base::badbit);
        }
    }
    return dest;
}


struct PollingMessage : Message {
	const string attribute;
	PollingMessage(const MessageType type_, 
			const string &attribute_)
		: Message(type_), attribute(attribute_) {}
	virtual ~PollingMessage() {}
};

struct PollingAttribute : PollingMessage {
	shared_ptr<Tango::DeviceProxy> dp;
	PollingAttribute(const string &attribute_,
			shared_ptr<Tango::DeviceProxy> dp_)
		: PollingMessage(SUBSCRIBE, attribute_), dp(dp_) {}
};

struct UnpollingAttribute : PollingMessage {
	UnpollingAttribute(const string &attribute_)
		: PollingMessage(UNSUBSCRIBE, attribute_) {}
};

struct Poll {
	string att_name, dev_fqdn;
	shared_ptr<Tango::DeviceProxy> dp;
	Poll(const string &att_name_,
			const string &dev_fqdn_,
			shared_ptr<Tango::DeviceProxy> dp_)
		: att_name(att_name_), dev_fqdn(dev_fqdn_), dp(dp_) {}
};

struct Subscriber::impl : public Tango::CallBack {
	shared_ptr<Channel<PollingMessage>> polling_channel;

	unordered_map<string /* attribute */,
		shared_ptr<Subscription>> attributes_subscribed;
	unordered_map<string /* attribute */,
		shared_ptr<Poll>> attributes_polled;

	Cache<Tango::DeviceProxy> proxies;

	thread poll_thread;
	Subscriber *suber;
	unsigned int ncalculators;
		
	shared_ptr<Channel<NotificationMessage>> notifier;

	impl(Subscriber *suberm, unsigned int ncalculators,
			shared_ptr<Channel<NotificationMessage>> notifier);

	void print_subscribed_structures() noexcept;
	void print_polled_structures() noexcept;
	void push_event(Tango::EventData *ev) noexcept;

	void handle(shared_ptr<PollingAttribute> mesg) noexcept;
	void handle(shared_ptr<UnpollingAttribute> mesg) noexcept;
	void handle(shared_ptr<PollingMessage> mesg) noexcept;

	void poll() noexcept;
};

int main() {
	notification_channel = make_shared<Channel<NotificationMessage>>();
	dispatching_channel = make_shared<Channel<DispatchingMessage>>();
	calculating_channels.push_back(make_shared<Channel<CalculatingMessage>>());
	subscribing_channel = make_shared<Channel<SubscriptionMessage>>();
	planning_channel = make_shared<Channel<PlanningMessage>>();
	receiving_channel = make_shared<Channel<Empty>>();

	unique_ptr<Subscriber> subscriber =
		make_unique<Subscriber>(subscribing_channel, 
				calculating_channels, notification_channel);
	
	vector<thread> threads;
	threads.emplace_back(bind(&Subscriber::loop, subscriber.get()));

	vector<pair<string, string>> attrs {
		{ "an attribute polled by Tango", 
			"tango://ken:20000/ken/tango/test/long_scalar" },
		{ "an attribute not polled by Tango", 
			"tango://ken:20000/ken/tango/test/double_scalar" },
		{ "a not existent attribute",
			"tango://ken:20000/pippo/pluto/paperino/topolino" },
	};

	{
		unsigned int cnt = 0, pcanoned = 0;
		for (auto a : attrs) {
			clog << "TEST: Subscribe " << a.first << endl;
		
			subscribing_channel->write(make_shared<SubscribeAttributes>
					(set<string>{a.second}));
			this_thread::sleep_for(chrono::milliseconds(200));
			assert(subscribing_channel->size() == 0);

			auto it = subscriber->pimpl->attributes_subscribed.find(a.second);
			if (! it->second->subscribed()) {
				++pcanoned;
			}

			assert(subscriber->pimpl->attributes_subscribed.size() == ++cnt);
			assert(subscriber->pimpl->polling_channel->size() == 0);
			assert(subscriber->pimpl->attributes_polled.size() == pcanoned);
		}
		cerr << endl;

		this_thread::sleep_for(chrono::seconds(2));
		vector<shared_ptr<CalculatingMessage>> cmesgs;
		calculating_channels[0]->read(cmesgs, 0);
		for (size_t i=0; i<cmesgs.size(); ++i) {
			auto cmesg = dynamic_pointer_cast<CalculatingDataMessage>(cmesgs[i]);
			switch(cmesg->type) {
				case CalculatingDataMessage::DOUBLEDATA: {
						auto it = dynamic_pointer_cast<DoubleData>(cmesg);
						cerr << "DATA " << it->attribute 
							<< " " << it->data << endl;
					 } break;
				case CalculatingDataMessage::INTDATA: {
						auto it = dynamic_pointer_cast<IntData>(cmesg);
						cerr << "DATA " << it->attribute 
							<< " " << it->data << endl;
					} break;
				default:
					assert(false);
			}
		}
	}

	{
		clog << "TEST: Unsubscribe all attributes" << endl;
		subscribing_channel->write(make_shared<UnsubscribeAttributes>
				(set<string>{attrs[0].second, 
				 attrs[1].second, attrs[2].second}));
		this_thread::sleep_for(chrono::milliseconds(200));
		assert(subscribing_channel->size() == 0);
		assert(subscriber->pimpl->attributes_subscribed.size() == 0);
		assert(subscriber->pimpl->attributes_polled.size() == 0);
	}

	subscribing_channel->close();
	threads[0].join();
}
