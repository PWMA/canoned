/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <thread>
#include <functional>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <cassert>
#include <unistd.h>
#include "../channel.h"
#include "../messages.h"

std::shared_ptr<Channel<StoreMessage>> dblogging_channel;
std::shared_ptr<Channel<NotificationMessage>> notification_channel;
std::shared_ptr<Channel<DispatchingMessage>> dispatching_channel;
std::vector<std::shared_ptr<Channel<CalculatingMessage>>> calculating_channels;
std::shared_ptr<Channel<SubscriptionMessage>> subscribing_channel;
std::shared_ptr<Channel<PlanningMessage>> planning_channel;
std::shared_ptr<Channel<Empty>> receiving_channel;
