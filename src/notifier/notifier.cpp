/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <unistd.h>
#include <nlohmann/json.hpp>
#include <curl/curl.h>
#include "notifier.h"

using namespace std;

struct Notifier::impl {
	const string smtpserver, adminemail, server_key;
	string senderemail, hostname, domainname;
	shared_ptr<Channel<StoreMessage>> dblogger;
	const bool foreground;

	impl(const string &smtpserver_, const string &adminemail_,
			const string &server_key_,
			shared_ptr<Channel<StoreMessage>> dblogger_,
			const bool foreground_)
		: smtpserver("smtp://" + smtpserver_), adminemail(adminemail_),
			server_key("Authorization: key=" + server_key_),
			dblogger(dblogger_), foreground(foreground_) {
		char tmp[1000];
		gethostname(tmp, 1000);
		string fullname = string(tmp);
		size_t pos = fullname.find('.');
		hostname = fullname.substr(0, pos);
		domainname = fullname.substr(pos+1);

		pos = adminemail.find('@');
		if (pos != string::npos) {
			senderemail = "noreply"
				+ adminemail.substr(pos);
		} 
	}

	void sendfcm(const string &label, const time_t time,
			const string &token, const string &body) noexcept;
	void sendemail(const string &email, const string &title, const string &body) noexcept;
	void sendadminemail(const string &title, const string &body) noexcept;
};

Notifier::Notifier(const string &smtpserver_, const string &adminemail_,
		const string &server_key_, 
		shared_ptr<Channel<NotificationMessage>> input_,
		shared_ptr<Channel<StoreMessage>> dblogger_,
		const bool foreground_)
	: NotifierBase(input_), pimpl{make_unique<impl>(smtpserver_,
			adminemail_, server_key_, dblogger_, foreground_)} {
		curl_global_init(CURL_GLOBAL_ALL);
}

Notifier::~Notifier() {
	curl_global_cleanup();
}

struct upload_status {
	int lines_read;
	vector<string> data;
};

static size_t payload_source(void *ptr, size_t size, size_t nmemb, void *userp)
{
	struct upload_status *upload_ctx = (struct upload_status *)userp;

	if((size == 0) || (nmemb == 0) || ((size*nmemb) < 1)) {
		return 0;
	}

	string data = upload_ctx->data[upload_ctx->lines_read];

	if(! data.empty()) {
		size_t len = data.size();
		memcpy(ptr, data.c_str(), len);
		upload_ctx->lines_read++;
		return len;
	}

	return 0;
}

size_t writefunc(void*, size_t size, size_t nmemb, void*) {
	return size*nmemb;
}

void Notifier::impl::sendfcm(const string &label, const time_t time,
		const string &token, const string &body) noexcept {
	if (server_key.empty())
		return;
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " send " << body << " as " 
		<< label << " to " << token << endl;
#endif

	string error;
	try {
		// TODO Use new Firebase's API format!
		nlohmann::json jmesg;
		jmesg["to"] = token;
		jmesg["priority"] = "high";
		jmesg["collapse_key"] = label;
		jmesg["content_available"] = true;
		jmesg["notification"]["title"] = label;
		jmesg["notification"]["body"] = body;
		jmesg["notification"]["sound"] = "default";
		jmesg["notification"]["tag"] = label;

		string payload = jmesg.dump();
		string content_length = "Content-Length: " 
			+ to_string(payload.size());

		struct curl_slist *list = NULL;
		CURL *curl = curl_easy_init();
		if (curl) {
#if 0
			curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#endif
			/* Prepare HTTP POST request */
			curl_easy_setopt(curl, CURLOPT_URL, 
					"https://fcm.googleapis.com/fcm/send");

			list = curl_slist_append(list, "Accept: */*");
			list = curl_slist_append(list, "Host: fcm.googleapis.com");
			list = curl_slist_append(list, "Content-Type: application/json");
			list = curl_slist_append(list, content_length.c_str());
			list = curl_slist_append(list, "Connection: close");
			list = curl_slist_append(list, server_key.c_str());
			curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);

			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload.c_str());
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);

			/* Perform the request */
			CURLcode res = curl_easy_perform(curl);
			curl_slist_free_all(list);

			if(res != CURLE_OK) {
				curl_easy_cleanup(curl);
				throw runtime_error(curl_easy_strerror(res));
			} else {
				long http_code = 0;
				curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE,
						&http_code);
				if (http_code != 200) {
					curl_easy_cleanup(curl);
					throw runtime_error(
							to_string(http_code));
				} else {
					cerr << __PRETTY_FUNCTION__ << " Notifiy "
						<< label << " " << time << " "
						<< token << endl;
					dblogger->write(make_shared<StoreAlarmNotify>
							(label, time, token));	
				}
			}
		} else {
			curl_easy_cleanup(curl);
			throw runtime_error("curl initialization failed");
		}
		curl_easy_cleanup(curl);
		return;
	} catch(exception &e) {
		error = e.what();
	} catch (...) {
		error = "Unknown error";
	}
	
	sendadminemail(__PRETTY_FUNCTION__, token + " " + error);
}

void Notifier::handle(shared_ptr<NotificationUserAlarm> mesg) noexcept {
	// TODO Bulk sending
	for( auto token : mesg->tokens) {
		if (pimpl->foreground) {
			clog << __PRETTY_FUNCTION__ << "Notify alarm for "
				<< mesg->label << " to " << token.first 
				<< " (" << token.second << ") "
				<< " with " << mesg->label << endl;
		} else { /* background */
			pimpl->sendfcm(mesg->label, mesg->time, token.first, mesg->body);

			string title = "[" + mesg->label + "] - alarm notification" ;
			string label_encoded = mesg->label;
			replace(label_encoded.begin(), label_encoded.end(), ' ', '+');
			string body = "Title: " + mesg->label + "\n"
				+ "Date: " + string(ctime((const time_t*)&mesg->time))
				+ "Note: " + mesg->body + "\n"
				+ "for further details look at: " + "https://" + pimpl->hostname + "."
					+ pimpl->domainname + "/pwma_starter.php?alarm_admin&alarm="
					+ label_encoded +"&token=" + token.first;
			pimpl->sendemail(token.second, title, body);
		}
	}
}

void Notifier::impl::sendemail(const string &email, const string &title, const string &body) noexcept {
	if (smtpserver.empty() || email.empty())
		return;
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " send " << body 
		<< " as " << title << endl;
#endif

	struct upload_status upload_ctx;
	vector<string> data {
		"From: Canoned on " + hostname + " <" + senderemail + ">\r\n",
		"To: " + email + "@" + domainname + "\r\n",
		"Subject: " + title + "\r\n",
		"\r\n",
		body + "\r\n",
		""
	};
	upload_ctx.data = data;
	upload_ctx.lines_read = 0;

	struct curl_slist *list = NULL;
	CURL *curl = curl_easy_init();
	if(curl) {
#if 0
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#endif
		/* Prepare email */
		curl_easy_setopt(curl, CURLOPT_URL, smtpserver.c_str());
		curl_easy_setopt(curl, CURLOPT_MAIL_FROM, senderemail.c_str());

		/* Add recipient */
		list = curl_slist_append(list, email.c_str());
		curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, list);

		/* Add payload */
		curl_easy_setopt(curl, CURLOPT_READFUNCTION, payload_source);
		curl_easy_setopt(curl, CURLOPT_READDATA, &upload_ctx);
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

		/* Send the message */
		CURLcode res = curl_easy_perform(curl);
		curl_slist_free_all(list);
		if(res != CURLE_OK) {
			cerr << __PRETTY_FUNCTION__ 
				<< " Error sending email" << endl;
		}
	}
	curl_easy_cleanup(curl);
}

void Notifier::impl::sendadminemail(const string &title, const string &body) noexcept {
	sendemail(adminemail, title, body);
}

void Notifier::handle(shared_ptr<NotificationSystemAlarm> mesg) noexcept {
	if (pimpl->foreground) {
		cerr << __PRETTY_FUNCTION__ << "Notify alarm for "
			<< mesg->title << " with " << mesg->body << endl;
	} else { /* background */
		pimpl->sendadminemail(mesg->title, mesg->body);
	}
}
