/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <string>
#include "../notifierbase.h"

class Notifier : public NotifierBase {
public:
	Notifier(const std::string &smtpserver, const std::string &adminemail,
			const std::string &server_key_,
			std::shared_ptr<Channel<NotificationMessage>> input,
			std::shared_ptr<Channel<StoreMessage>> dblogger,
			const bool foreground);
	~Notifier();

	void handle(std::shared_ptr<NotificationUserAlarm> mesg) noexcept;
	void handle(std::shared_ptr<NotificationSystemAlarm> mesg) noexcept;
#ifdef TEST
public:
#else
private:
#endif
	class impl;
	std::unique_ptr<impl> pimpl;
};
