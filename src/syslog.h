/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string> 
#include <ostream> 

enum syslog_priority { SYSLOG_DEBUG = 7, SYSLOG_ERROR = 3 };

class Syslog : public std::basic_streambuf<char, std::char_traits<char> > {
public:
	Syslog(syslog_priority priority);
	~Syslog();
private:
	std::string buffer;
	syslog_priority priority;

	int sync() noexcept;
	int overflow(int c) noexcept;
};
