/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include "dispatcherbase.h"

using namespace std;

DispatcherBase::DispatcherBase(shared_ptr<Channel<DispatchingMessage>> input_,
		shared_ptr<Channel<NotificationMessage>> notifier_,
		shared_ptr<Channel<StoreMessage>> dblogger_)
	: input(input_), notifier(notifier_), dblogger(dblogger_) {}

DispatcherBase::~DispatcherBase() {}

void DispatcherBase::handle(shared_ptr<DispatchingMessage> mesg) noexcept {
	switch (mesg->type) {
		case Message::ACK:
			pass(handle(dynamic_pointer_cast
					<DispatchingAck>(mesg)));
			break;
		case Message::ADDCONFIG:
			pass(handle(dynamic_pointer_cast
					<DispatchingAddConfig>(mesg)));
			break;
		case Message::REMOVECONFIG:
			pass(handle(dynamic_pointer_cast
					<DispatchingRemoveConfig>(mesg)));
			break;
		case Message::UPDATE:
			pass(handle(dynamic_pointer_cast
					<DispatchingUpdate>(mesg)));
			break;
		default:
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__,
					"Message not handled:"
					+ to_string(mesg->type)));
			return;
	}
}

void DispatcherBase::pass(Result result) noexcept {
	if (result.smesg)
		dblogger->write(result.smesg);
	if (result.nmesg)
		notifier->write(result.nmesg);
}

void DispatcherBase::setup() noexcept {}

time_t DispatcherBase::prepare() noexcept {
	return 0xFFFFFFFF;
}

void DispatcherBase::timeout() noexcept {
	notifier->write(make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Wrong invocation"));
}

void DispatcherBase::close() noexcept {}

void DispatcherBase::loop() noexcept {
	pthread_setname_np(pthread_self(), "dispatcher");

	setup();

	while(true) {
		time_t wait_until = prepare();

		vector<shared_ptr<DispatchingMessage>> mesgs;
		switch (input->read(mesgs, wait_until))
		{
			case CLOSE:
				close();
				return;
			case TIMEOUT:
				timeout();
				continue;
			case DATA:
				for (auto mesg : mesgs)
					handle(mesg);
		}
	}
}
