/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include "dbloggerbase.h"

using namespace std;

DbLoggerBase::DbLoggerBase(shared_ptr<Channel<StoreMessage>> input_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: input(input_), notifier(notifier_) {}

DbLoggerBase::~DbLoggerBase() {}

void DbLoggerBase::handle(shared_ptr<StoreMessage> mesg) noexcept {
	switch (mesg->type) {
		case Message::ADDCONFIG:
			handle(dynamic_pointer_cast
					<StoreAddConfig>(mesg));
			break;
		case Message::ACK:
			handle(dynamic_pointer_cast
					<StoreAlarmAck>(mesg));
			break;
		case Message::SUBSCRIBE:
			handle(dynamic_pointer_cast
					<StoreAlarmAdd>(mesg));
			break;
		case Message::UNSUBSCRIBE:
			handle(dynamic_pointer_cast
					<StoreAlarmRemove>(mesg));
			break;
		case Message::UPDATE:
			handle(dynamic_pointer_cast
					<StoreAlarmNotify>(mesg));
			break;
		default:
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__, 
					 "Message not handled:"
					 + to_string(mesg->type)));
			return;
	}
}

void DbLoggerBase::setup() noexcept {}

time_t DbLoggerBase::prepare() noexcept {
	return 0xFFFFFFFF;
}

void DbLoggerBase::timeout() noexcept {
	notifier->write(make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Wrong invocation"));
}

void DbLoggerBase::close() noexcept {}

void DbLoggerBase::loop() noexcept {
	pthread_setname_np(pthread_self(), "dblogger");

	setup();

	while(true) {
		time_t wait_until = prepare();

		vector<shared_ptr<StoreMessage>> mesgs;
		switch (input->read(mesgs, wait_until))
		{
			case CLOSE:
				close();
				return;
			case TIMEOUT:
				timeout();
				continue;
			case DATA:
				for (auto mesg : mesgs)
					handle(mesg);
		}
	}
}
