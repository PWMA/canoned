/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include "notifierbase.h"

using namespace std;

NotifierBase::NotifierBase(shared_ptr<Channel<NotificationMessage>> input_)
	: input(input_) {}

NotifierBase::~NotifierBase() {}

void NotifierBase::handle(shared_ptr<NotificationMessage> mesg) noexcept {
	switch (mesg->type) {
		case Message::SENDUSERALARM:
			handle(dynamic_pointer_cast
					<NotificationUserAlarm>(mesg));
			break;
		case Message::SENDSYSTEMALARM:
			handle(dynamic_pointer_cast
					<NotificationSystemAlarm>(mesg));
			break;
		default:
			handle(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__, 
					 "Message not handled:"
					 + to_string(mesg->type)));
			return;
	}
}

void NotifierBase::setup() noexcept {}

time_t NotifierBase::prepare() noexcept {
	return 0xFFFFFFFF;
}

void NotifierBase::timeout() noexcept {
	handle(make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Wrong invocation"));
}

void NotifierBase::close() noexcept {}

void NotifierBase::loop() noexcept {
	pthread_setname_np(pthread_self(), "notifier");

	setup();

	while(true) {
		time_t wait_until = prepare();

		vector<shared_ptr<NotificationMessage>> mesgs;
		switch (input->read(mesgs, wait_until))
		{
			case CLOSE:
				close();
				return;
			case TIMEOUT:
				timeout();
				continue;
			case DATA:
				for (auto mesg : mesgs)
					handle(mesg);
		}
	}
}
