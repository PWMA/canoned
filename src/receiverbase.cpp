/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include "receiverbase.h"

using namespace std;

ReceiverBase::ReceiverBase(shared_ptr<Channel<Empty>> input_,
		shared_ptr<Channel<PlanningMessage>> planner_,
		shared_ptr<Channel<DispatchingMessage>> dispatcher_,
		shared_ptr<Channel<NotificationMessage>> notifier_,
		shared_ptr<Channel<StoreMessage>> dblogger_)
	: input(input_), planner(planner_), dispatcher(dispatcher_),
		notifier(notifier_), dblogger(dblogger_) {}

ReceiverBase::~ReceiverBase() {}

void ReceiverBase::pass(Result result) noexcept {
	if (result.smesg)
		dblogger->write(result.smesg);
	if (result.nmesg)
		notifier->write(result.nmesg);
	if (result.dmesg)
		dispatcher->write(result.dmesg);
	if (result.pmesg)
		planner->write(result.pmesg);
}

void ReceiverBase::setup() noexcept {}

time_t ReceiverBase::prepare() noexcept {
	return 0xFFFFFFFF;
}

void ReceiverBase::timeout() noexcept {
	notifier->write(make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Wrong invocation"));
}

void ReceiverBase::close() noexcept {
	notifier->write(make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Wrong invocation"));
}

void ReceiverBase::loop() noexcept {
	pthread_setname_np(pthread_self(), "receiver");

	setup();

	while(true) {
		time_t wait_until = prepare();

		vector<shared_ptr<Empty>> mesgs;
		switch (input->read(mesgs, wait_until))
		{
			case CLOSE:
				close();
				return;
			case TIMEOUT:
				timeout();
				continue;
			case DATA:
				for (auto mesg : mesgs)
					pass(handle(mesg));
		}
	}
}
