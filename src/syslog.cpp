/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "syslog.h"
#include <syslog.h>

using namespace std;

Syslog::Syslog(syslog_priority priority_) : priority(priority_) {
	openlog("canoned", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);
}

Syslog::~Syslog() {
	closelog();
}

int Syslog::sync() noexcept {
	if (buffer.length()) {
		::syslog(priority, "%s", buffer.c_str());
		buffer.erase();
	}
	return 0;
}

int Syslog::overflow(int c) noexcept {
	if (c != EOF && c != 10 /* LF */) {
		buffer += static_cast<char>(c);
	} else {
		sync();
	}
	return c;
}
