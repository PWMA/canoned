/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <cstring>
#include <sstream>
#include <iostream>
#include <thread>
#include <functional>
#include <nlohmann/json.hpp>
#include <libpq-fe.h>
#include "receiver.h"

using namespace std;

struct Receiver::impl {
	const string conninfo;
	int pipefd[2];
	PGconn *conn;
	PGnotify *notify;
	thread receiving_thread;
	Receiver *recv;
	shared_ptr<Channel<NotificationMessage>> notifier;

	impl(Receiver *recv_, const string &conninfo_,
			shared_ptr<Channel<NotificationMessage>> notifier_);
	~impl();

	void forward_alarm(const string &mesg, const bool firstconf) noexcept;
	void forward_token(const string &mesg) noexcept;
	void receive() noexcept;
};

Receiver::Receiver(const string &conninfo_, 
		shared_ptr<Channel<Empty>> input_, 
		shared_ptr<Channel<PlanningMessage>> planner_,
		shared_ptr<Channel<DispatchingMessage>> dispatcher_,
		shared_ptr<Channel<NotificationMessage>> notifier_,
		shared_ptr<Channel<StoreMessage>> dblogger_)
	: ReceiverBase(input_, planner_, dispatcher_, notifier_, dblogger_), 
		pimpl{make_unique<impl>(this, conninfo_, notifier_)} {}
Receiver::~Receiver() {}

Receiver::impl::impl(Receiver *recv_, const string &conninfo_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: conninfo(conninfo_), conn(NULL), notify(NULL), recv(recv_), 
		notifier(notifier_) {
	if (pipe(pipefd) == -1) {
		stringstream emesg;
		emesg << "Unable to create pipe: "
			<< strerror(errno) << endl;
		notifier->write(make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, emesg.str()));
	}
}
Receiver::impl::~impl() {}

void Receiver::impl::forward_alarm(const string &mesg_, const bool firstconf) noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " " << mesg_ << endl;
#endif
	string mesg = mesg_;
	nlohmann::json jmesg;
	string token, label, formula, note, error, username;
	try {
		/* Remove characters */
		for (auto it = mesg.begin(); it != mesg.end(); ) {
			if((int)*it<32 || (int)*it>126) {
				it = mesg.erase(it);
			} else 
				++it;
		}
		
		/* Parse JSON, check fields and forward to relevant channels */
		jmesg = nlohmann::json::parse(mesg);
		token = jmesg["data"]["token"].get<string>();

		string event = jmesg["event"].get<string>();
		label = jmesg["data"]["label"].get<string>();

		if((event != "add" && event != "remove" && event != "ack") 
				|| token.empty() || label.empty())
			throw runtime_error("Invalid or missing"
					" mandatory data");

		ReceiverBase::Result result;
		if (event == "add") {	
			formula = jmesg["data"]["formula"].get<string>();
			transform(formula.begin(), formula.end(), 
					formula.begin(), ::tolower);
		
			if (formula.empty())
				throw runtime_error("Formula is empty!");

			long delay;
			try {
				delay = jmesg["data"]["delay"].get<long>();
			} catch(...) {
				delay = 0L;
			}

			bool ack;
			try {
				ack = jmesg["data"]["ack"].get<bool>();
			} catch(...) {
				ack = true;
			}

			try {
				username = jmesg["data"]["username"].get<string>();
			} catch(...) {}

			note = jmesg["data"]["note"].get<string>();

			result.pmesg = make_shared<PlanningSubscribe>
				(label, token, formula, note, delay,
				 username, ack, firstconf);
		} else if (event == "remove") {
			result.pmesg = make_shared<PlanningUnsubscribe>
				(label, token);
		} else if (event == "ack") {
			result.dmesg = make_shared<DispatchingAck>
				(label, token);
		} else {
			result.nmesg = make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, "Wrong event: " + event);
		}
		recv->pass(result);
		return;
	} catch(exception &e) {
		error = e.what();
	} catch(...) {
		error = "Unknown error";
	}

	stringstream emesg;
	emesg << "Fails to forward" << mesg
		<< " due " << error << endl;
	notifier->write(make_shared<NotificationSystemAlarm>
		(__PRETTY_FUNCTION__, emesg.str()));
}

void Receiver::impl::forward_token(const string &mesg_) noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " " << mesg_ << endl;
#endif
	
	string mesg = mesg_;

	ReceiverBase::Result result;
	nlohmann::json jmesg;
	string error;
	try {
		/* Remove characters */
		for (auto it = mesg.begin(); it != mesg.end(); ) {
			if((int)*it<32 || (int)*it>126) {
				it = mesg.erase(it);
			} else 
				++it;
		}
		
		/* Parse JSON, check fields and forward to relevant channels */
		jmesg = nlohmann::json::parse(mesg);
		string event = jmesg["event"].get<string>();
		if (event == "add") {	
			string token = jmesg["data"]["token"].get<string>();
			int tokenid = jmesg["data"]["id"].get<int>();

			if(token.empty())
				throw runtime_error("Invalid or missing"
						" mandatory data");
			
			result.smesg = make_shared<StoreAddConfig>
				(token, tokenid);
		} else {
			result.nmesg = make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, "Wrong event: " + event);
		}
		recv->pass(result);
		return;
	} catch(exception &e) {
		error = e.what();
	} catch(...) {
		error = "Unknown error";
	}
	
	stringstream emesg;
	emesg << "Fails to forward" << mesg
		<< " due " << error << endl;
	notifier->write(make_shared<NotificationSystemAlarm>
		(__PRETTY_FUNCTION__, emesg.str()));
}

void Receiver::impl::receive() noexcept {
	while(true) {
		/* Sleep a bit to avoid to busy-loop 
		 * against a not responding PostgreSQL */
		this_thread::sleep_for(chrono::milliseconds(500));
#ifndef NDEBUG
		clog << __PRETTY_FUNCTION__ << " connecting to the database" << endl;
#endif
		conn = PQconnectdb(conninfo.c_str());
		if (PQstatus(conn) != CONNECTION_OK) {
			stringstream emesg;
			emesg << "Database connection failed: " << PQerrorMessage(conn);
			PQfinish(conn);
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__, emesg.str()));
			continue;
		}

		PGresult *res = PQexec(conn, "LISTEN alarm; LISTEN token");
		if (PQresultStatus(res) != PGRES_COMMAND_OK)
		{
			stringstream emesg;
			emesg << "Listen command failed: " << PQerrorMessage(conn) << endl;
			PQclear(res);
			PQfinish(conn);
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__, emesg.str()));
			continue;
		}
		PQclear(res);

#ifndef NDEBUG
		clog << __PRETTY_FUNCTION__ << " retrieve initial data" << endl;
#endif

		res = PQexec(conn, "SELECT id, value, username FROM token");
		if(PQresultStatus(res) != PGRES_TUPLES_OK) {
			stringstream emesg;
			emesg << "Initial retrieve token data failed: " << PQerrorMessage(conn) << endl;
			PQclear(res);
			PQfinish(conn);
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__, emesg.str()));
			continue;
		}

		for (int row=0; row<PQntuples(res); ++row) {
			string tokenid(PQgetvalue(res, row, 0), PQgetlength(res, row, 0));
			string token(PQgetvalue(res, row, 1), PQgetlength(res, row, 1));

			forward_token("{\"event\":\"add\", \"data\":{\"id\":"
					+ tokenid + ", " + "\"token\": \""
					+ token + "\"}}" );
		}
		PQclear(res);

		res = PQexec(conn, "SELECT DISTINCT ON (formula.label, token.value) formula.label, "
				"formula.formula, token.value, formula.delay, formula.note, "
				"token.username, alarmlog.ack FROM alarm "
				"INNER JOIN formula ON formula.label = alarm.label "
				"INNER JOIN token ON token.id = alarm.tokenid "
				"INNER JOIN alarmlog ON alarmlog.label = alarm.label "
				"AND alarmlog.tokenid = alarm.tokenid "
				"ORDER BY formula.label, token.value, alarmlog.date DESC");
		if(PQresultStatus(res) != PGRES_TUPLES_OK) {
			stringstream emesg;
			emesg << "Initial retrieve alarm data failed: " << PQerrorMessage(conn) << endl;
			PQclear(res);
			PQfinish(conn);
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__, emesg.str()));
			continue;
		}

		for (int row=0; row<PQntuples(res); ++row) {
			string label(PQgetvalue(res, row, 0), PQgetlength(res, row, 0));
			string formula(PQgetvalue(res, row, 1), PQgetlength(res, row, 1));
			string token(PQgetvalue(res, row, 2), PQgetlength(res, row, 2));
			string delay(PQgetvalue(res, row, 3), PQgetlength(res, row, 3));
			string note(PQgetvalue(res, row, 4), PQgetlength(res, row, 4));
			string username(PQgetvalue(res, row, 5), PQgetlength(res, row, 5));
			bool ack = string(PQgetvalue(res, row, 6), PQgetlength(res, row, 6)) == "t"? true : false;

			if (delay.empty())
				delay = "0";

			forward_alarm("{\"event\":\"add\", \"data\":{\"label\": \"" + label + "\", "
					+ "\"token\": \"" + token + "\", \"formula\": \"" 
					+ formula + "\", \"delay\": " + delay + ", "
					+ "\"note\": \"" + note + "\", \"username\": \"" 
					+ username + "\", \"ack\": " + (ack? "true" : "false") + "}}" , true);
		}
		PQclear(res);

		while ((PQping(conninfo.c_str()) == PQPING_OK)) {
			int         sock;
			fd_set      input_mask;

			sock = PQsocket(conn);
			if (sock < 0) {
				stringstream emesg;
				emesg << " PQsocket() failed: " << PQerrorMessage(conn) << endl;
				notifier->write(make_shared<NotificationSystemAlarm>
						(__PRETTY_FUNCTION__, emesg.str()));
				continue;
			}
#ifndef NDEBUG
			clog << __PRETTY_FUNCTION__ << " is waiting new data" << endl;
#endif
			FD_ZERO(&input_mask);
			FD_SET(sock, &input_mask);
			FD_SET(pipefd[0], &input_mask);
			int maxfd = pipefd[0] > sock? pipefd[0] : sock;
			if (select(maxfd + 1, &input_mask, NULL, NULL, NULL) < 0)
			{
				stringstream emesg;
				emesg << "Select() failed: " << PQerrorMessage(conn) << endl;
				notifier->write(make_shared<NotificationSystemAlarm>
						(__PRETTY_FUNCTION__, emesg.str()));
				continue;
			}

			if (FD_ISSET(pipefd[0], &input_mask)) {
				return;
			}

			PQconsumeInput(conn);
			while ((notify = PQnotifies(conn)) != NULL)
			{
				string channel_name = string(notify->relname);
				if (channel_name == "alarm") {
					forward_alarm(notify->extra, false);
				} else if (channel_name == "token") {
					forward_token(notify->extra);
				} else {
					notifier->write(make_shared<NotificationSystemAlarm>
							(__PRETTY_FUNCTION__, "Wrong channel" 
							 + channel_name));
				}
				PQfreemem(notify);
			}
		}
#ifndef NDEBUG
		clog << __PRETTY_FUNCTION__ << " disconnecting from the database" << endl;
#endif
		PQfinish(conn);
	}
}

ReceiverBase::Result Receiver::handle(shared_ptr<Empty>) noexcept {
	ReceiverBase::Result result;
	result.nmesg = make_shared<NotificationSystemAlarm>
		(__PRETTY_FUNCTION__, "Empty message");
	return result;
}

void Receiver::setup() noexcept {
	pimpl->receiving_thread = thread(bind(
				&Receiver::impl::receive, 
				pimpl.get()));
}

void Receiver::close() noexcept {
	/* Interrupt select() */
	int d = 1;
	ssize_t ret = write(pimpl->pipefd[1], &d, sizeof(d));
	(void)ret;
	
	if (pimpl->receiving_thread.joinable())
		pimpl->receiving_thread.join();
}
