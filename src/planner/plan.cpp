/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sstream>
#include  "plan.h"

using namespace std;

Plan::Plan(const set<string> &attributes_,
		const set<string> &tokens_,
		const unsigned int calc_id_)
	: calc_id(calc_id_), attributes(attributes_),
		tokens(tokens_) {}

Plan::~Plan() {}

bool Plan::add_token(const string &token_) noexcept {
	auto it = tokens.insert(token_);
	return it.second;
}
	
bool Plan::remove_token(const string &token_) noexcept {
	return tokens.erase(token_);
}

size_t Plan::tokens_count() noexcept {
	return tokens.size();
}

const set<string>& Plan::get_attributes() noexcept {
	return attributes;
}

unsigned int Plan::get_calc_id() noexcept {
	return calc_id;
}

ostream& operator<<(ostream& dest, const Plan &value) {
	stringstream log;
	log << " CalcID: " << to_string(value.calc_id)
		<< " Attributes:";
	for (auto a : value.attributes)
		log << " " << a;
	log << " Tokens:";
	for (auto t : value.tokens)
		log << " " << t;

	dest << log.str();

	return dest;
}
