/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>
#include <set>
#include <unordered_map>
#ifndef NDEBUG
#include <sstream>
#endif
#include <iostream>
#include "plan.h"
#include "planner.h"

using namespace std;

set<string> extract_attributes(const string &formula) {
	set<string> attributes;
	size_t pos = 0;
	while((pos = formula.find("tango://", pos)) != string::npos) {
		size_t start = pos;
		pos += 8;
		for( int i=0; i<4; ++i) {
			pos = formula.find("/", pos+1);
		}
		pos = formula.find_first_not_of("abcdefghijklmnopqrstuvwxyz"
				"0123456789_.", pos+1);
		attributes.insert(formula.substr(start, pos - start));
	}
	return attributes;
}

struct Planner::impl {
	/* Calculators' index (Round robin) */
	unsigned int calc_id;
	const unsigned int max_calc_id;
	unsigned int get_next_calc_id() noexcept;

	unordered_map<string /* label */, 
		shared_ptr<Plan>> plans;
	unordered_map<string /* attribute */, 
		long /* Reference counter */> attributes;

	impl(unsigned int max_calc_id_);
	~impl();

	void print_structures() noexcept;
};

Planner::Planner(shared_ptr<Channel<PlanningMessage>> input_,
		shared_ptr<Channel<SubscriptionMessage>> subscriber_,
		vector<shared_ptr<Channel<CalculatingMessage>>> &calculators_,
		shared_ptr<Channel<DispatchingMessage>> dispatcher_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: PlannerBase(input_, subscriber_, calculators_, dispatcher_, notifier_)
	  , pimpl{make_unique<impl>(calculators_.size())} {}

Planner::~Planner() {}

Planner::impl::impl(unsigned int max_calc_id_) 
	: calc_id(0), max_calc_id(max_calc_id_) {}

Planner::impl::~impl() {}

unsigned int Planner::impl::get_next_calc_id() noexcept {
	if (calc_id < max_calc_id)
		return calc_id++;
	calc_id = 0;
	return calc_id++;
}

void Planner::impl::print_structures() noexcept {
#ifndef NDEBUG
	stringstream log;
	log << __PRETTY_FUNCTION__;
	for (auto f : plans) {
		log << " Label: " << f.first
			<< " " << *f.second.get()
			<< endl;
	}
	
	log << __PRETTY_FUNCTION__;
	for (auto a : attributes) {
		log << " Attribute: " << a.first
			<< " RefCounter: " << a.second
			<< endl;
	}
	clog << log.str();
#endif
}

PlannerBase::Result Planner::handle(shared_ptr<PlanningSubscribe> mesg) noexcept {
#ifndef NDEBUG	
	clog << __PRETTY_FUNCTION__ << " add " << mesg->label << " (" 
		<< mesg->formula << ") for " << mesg->token << " with delay of "
		<< mesg->delay << " with ack " << mesg->ack << endl;
#endif
	
	PlannerBase::Result result;

	auto it = pimpl->plans.find(mesg->label);
	if (it != pimpl->plans.end()) {
		/* Plan is already registered so add the new token. 
		 * If it isn't duplicated send a message to dispatcher 
		 * about this new token */
		if (it->second->add_token(mesg->token)) {
			result.dmesg = make_shared<DispatchingAddConfig>
				(mesg->label, mesg->note, mesg->token,
				 mesg->delay, mesg->username, mesg->ack, mesg->firstconf);
		}
	} else {
		/* Plan first registration: send a message to dispatcher to update
		 * its delivery table, create a Plan c++ object and assign it 
		 * to a specific calculator and finally send a message to subscriber
		 * to subscribe all attributes involved */
		result.dmesg = make_shared<DispatchingAddConfig>
			(mesg->label, mesg->note, mesg->token, mesg->delay,
			 mesg->username, mesg->ack, mesg->firstconf);

		set<string> formula_attributes = extract_attributes(mesg->formula);

		unsigned int cid = pimpl->get_next_calc_id();
		it = pimpl->plans.emplace(mesg->label, 
				make_shared<Plan>(formula_attributes,
					set<string>{mesg->token},
					cid)).first;
		for (unsigned int i=0; i<cid; ++i)
			result.cmesgs.push_back({});
		result.cmesgs.push_back(make_shared<CalculatingAddConfig>
				(it->first, mesg->formula, formula_attributes));

		/* Check every attribute to see if if is a already available in 
		 * our global list. If it is the case increment the counter 
		 * otherwise add it with counter set to 1 */
		set<string> to_be_subscribed;
		for (auto attribute : formula_attributes) {
			auto ait = pimpl->attributes.find(attribute);
			if (ait != pimpl->attributes.end()) {
				ait->second++;
			} else {
				pimpl->attributes.emplace(attribute, 1);
				to_be_subscribed.insert(attribute);
			}
		}

		/* Some new attributes to subscribe */
		if (to_be_subscribed.size()) {
			result.smesg = make_shared<SubscribeAttributes>
				(to_be_subscribed);
		}
	}

	pimpl->print_structures();

	return result;
}

PlannerBase::Result Planner::handle(shared_ptr<PlanningUnsubscribe> mesg) noexcept {
#ifndef NDEBUG	
	clog << __PRETTY_FUNCTION__ << " remove " << mesg->label << " for " 
		<< mesg->token << endl;
#endif

	PlannerBase::Result result;

	auto it = pimpl->plans.find(mesg->label);
	if (it != pimpl->plans.end()) {
		/* Plan is already registered so remove the token. 
		 * If it is _actually_ removed (aka it isn't a removing 
		 * duplicated request) send a message to dispatcher 
		 * about this just removed token */
		if (it->second->remove_token(mesg->token))  {
			result.dmesg = make_shared<DispatchingRemoveConfig>
				(mesg->label, mesg->token);
		}

		/* If the tokens' count reach zero we have to remove the 
		 * Plan c++ object: send a message to the specific calculator
		 * to remove from that too and reduce by one the refcounter
		 * of every attributes involved */
		if (! it->second->tokens_count()) {
			set<string> formula_attributes 
				= it->second->get_attributes();

			for (unsigned int i=0; i<it->second->get_calc_id(); ++i)
				result.cmesgs.push_back({});
			result.cmesgs.push_back(make_shared<CalculatingRemoveConfig>
					(it->first, formula_attributes));
		
			set<string> to_be_unsubscribed;
			for (auto attribute : formula_attributes) {
				auto ait = pimpl->attributes.find(attribute);
				if (ait != pimpl->attributes.end()) {
					ait->second--;
					/* Attribute refcounter reach zero 
					 * so remove it altogether */
					if (! ait->second) {
						to_be_unsubscribed.insert(attribute);
						pimpl->attributes.erase(ait);
					}
				} else {
					result.nmesg = make_shared<NotificationSystemAlarm>
						(__PRETTY_FUNCTION__, "Logic error");
				}
			}	

			/* Some old attributes to unsubscribe */
			if (to_be_unsubscribed.size()) {
				result.smesg = make_shared<UnsubscribeAttributes>
					(to_be_unsubscribed);
			}
			pimpl->plans.erase(it);
		}
	} 

	pimpl->print_structures();

	return result;
}
