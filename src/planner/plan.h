/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <set>

class Plan {
public:	
	Plan(const std::set<std::string> &attributes_,
			const std::set<std::string> &tokens_,
			const unsigned int calc_id_);
	~Plan();

	bool add_token(const std::string &token_) noexcept;
	bool remove_token(const std::string &token_) noexcept;
	size_t tokens_count() noexcept;

	const std::set<std::string>& get_attributes() noexcept;

	unsigned int get_calc_id() noexcept;

	friend std::ostream& operator<<(std::ostream& dest, const Plan &value);
#ifdef TEST
public:
#else
private:
#endif
	const unsigned int calc_id;
	const std::set<std::string> attributes;
	std::set<std::string> tokens;
};

std::ostream& operator<<(std::ostream& dest, const Plan &value);
