/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <unordered_map>
#include <memory>
#include <set>
#include <mpParser.h>

class Formula {
public:
	Formula(const std::string &label, const std::string &formula,
			const std::set<std::string> &attributes);

	const std::string& get_label() noexcept;

	mup::Value update(const std::string &attribute, mup::Value &new_value);
private:
	const std::string label;
	std::string formula;
	std::unordered_map<std::string /* attribute */, 
		std::pair<std::string /* attribute_sanitized */, 
		std::shared_ptr<mup::Value>>> attributes;

	std::shared_ptr<mup::ParserX> parser;
	
	friend std::ostream& operator<<(std::ostream& dest, const Formula &value);
};

std::ostream& operator<<(std::ostream& dest, const Formula &value);
