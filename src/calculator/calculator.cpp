/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>
#include <unordered_map>
#include <iostream>
#include <mpParser.h>
#include "formula.h"
#include "calculator.h"

using namespace std;

struct Calculator::impl {
	unordered_multimap<string /* attribute */, 
			shared_ptr<Formula>> operands;

	void print_structures() noexcept;

	template<typename T> CalculatorBase::Result calc(shared_ptr<T> mesg) noexcept;
};

Calculator::Calculator(unsigned int id_,
		shared_ptr<Channel<CalculatingMessage>> input_, 
		shared_ptr<Channel<DispatchingMessage>> dispatcher_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: CalculatorBase(id_, input_, dispatcher_, notifier_),
		pimpl{make_unique<impl>()} {}

Calculator::~Calculator() {}

void Calculator::impl::print_structures() noexcept {
#ifndef NDEBUG
	set<shared_ptr<Formula>> formulas;
	for(auto op : operands) {
		formulas.insert(op.second);
	}

	stringstream log;
	log << __PRETTY_FUNCTION__;
	for(auto f : formulas) {
		log << *f.get() << endl;
	}
	clog << log.str();
#endif
}

template<typename T> CalculatorBase::Result Calculator::impl::calc(
		shared_ptr<T> mesg) noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " calculate result for "
		<< mesg->attribute << endl;
#endif

	CalculatorBase::Result result;

	/* Find all formulas which include the attribute changed */
	auto o_it = operands.equal_range(mesg->attribute);
	for (auto it = o_it.first; it != o_it.second; ++it) {
		bool outcome = false;
		string error;
		mup::Value update = mesg->data;
		
		try {
			/* Update attribute value */
			mup::Value value = it->second->update
				(mesg->attribute, update);

			/* Check return type and forward the outcome */	
			if (value.GetType() != 'b') {
				// Types are 'b', 's', 'f', 'i', 'm' and 'c'
				error = "Invalid result type";
			} else {
				outcome = value.GetBool();
			}
		} catch (mup::ParserError &e) {
			if (e.GetCode() == mup::ecUNASSIGNABLE_TOKEN) {
				continue;
			}
			error = e.GetMsg();
		} catch( exception &e ) {
			error = e.what();
		} catch(...) {
			error = "Unknown error";
		}

		if (! error.empty()) {
			result.nmesg = make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, mesg->attribute
				 + " " + it->second->get_label()
				 + " " + error);
		} else {
			result.dmesgs.push_back(make_shared<DispatchingUpdate>
				(it->second->get_label(), mesg->time, outcome));
		}
	}
	
	return result;
}

CalculatorBase::Result Calculator::handle(shared_ptr<CalculatingAddConfig> mesg)
		noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " add " << mesg->label 
		<< " as " << mesg->formula << " " << endl;
#endif

	CalculatorBase::Result result;

	/* Create formula and add it to all attributes which are
	 * referrered by that formula */
	auto f = make_shared<Formula>(mesg->label, mesg->formula, 
			mesg->attributes);

	for (auto attribute : mesg->attributes) {
		pimpl->operands.emplace(attribute, f);
	}

	pimpl->print_structures();

	return result;
}

CalculatorBase::Result Calculator::handle(shared_ptr<CalculatingRemoveConfig> mesg)
		noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " remove " << mesg->label << endl;
#endif

	CalculatorBase::Result result;

	/* Remove all attributes which are referrered by that formula */
	for (auto attribute : mesg->attributes) {
		auto o_it = pimpl->operands.equal_range(attribute);
		for (auto it = o_it.first; it != o_it.second;) {
			if (it->second->get_label() == mesg->label) {
				it = pimpl->operands.erase(it);
			} else
				++it;
		}
	}

	pimpl->print_structures();

	return result;
}

CalculatorBase::Result Calculator::handle(shared_ptr<CalculatingDataMessage> mesg)
		noexcept {
	CalculatorBase::Result result;	
	switch (mesg->type) {
		case CalculatingDataMessage::BOOLDATA:
			result = pimpl->calc(dynamic_pointer_cast<BoolData>(mesg));
			break;
		case CalculatingDataMessage::DOUBLEDATA:
			result = pimpl->calc(dynamic_pointer_cast<DoubleData>(mesg));
			break;
		case CalculatingDataMessage::STRINGDATA:
			result = pimpl->calc(dynamic_pointer_cast<StringData>(mesg));
			break;
		case CalculatingDataMessage::INTDATA:
			result = pimpl->calc(dynamic_pointer_cast<IntData>(mesg));
			break;
		default:
			result.nmesg = make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, "Message not handled:"
				 + to_string(mesg->type));
	}
	return result;
}
