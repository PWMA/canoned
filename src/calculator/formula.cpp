/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "formula.h"

using namespace std;

Formula::Formula(const string &label_, const string &formula_,
		const set<string> &attributes_) 
	: label(label_), formula(formula_) {
	/* Setup Parser */
	parser = make_shared<mup::ParserX>(mup::pckCOMMON | mup::pckCOMPLEX 
			| mup::pckSTRING | mup::pckUNIT | mup::pckMATRIX);

	/* Replace special characters which are valid in Tango naming
	 * but can't be used in mathematical formulas */
	for (auto attribute : attributes_) {
		string attribute_sanitized = attribute;
		replace(attribute_sanitized.begin(), 
				attribute_sanitized.end(), '-', '_');
		replace(attribute_sanitized.begin(), 
				attribute_sanitized.end(), '/', '_');
		replace(attribute_sanitized.begin(), 
				attribute_sanitized.end(), ':', '_');
		replace(attribute_sanitized.begin(), 
				attribute_sanitized.end(), '.', '_');

		size_t pos = 0;
		do {
			pos = formula.find(attribute, pos);
			if (pos == string::npos)
				break;
			formula.replace(pos, attribute.size(), 
					attribute_sanitized);
		} while(true);

		shared_ptr<mup::Value> value;
		attributes.emplace(attribute, 
				make_pair(attribute_sanitized, value));
	}

	try {
		parser->SetExpr(formula);
	} catch(...) { /* Ignore any error */ }
}

const string& Formula::get_label() noexcept {
	return label;
}

mup::Value Formula::update(const string &attribute, 
		mup::Value &new_value) {
	auto it = attributes.find(attribute);
	if (it != attributes.end()) { 
		if (it->second.second) {
			/* Update value */
			*it->second.second = new_value;
		} else {
			/* First time so variable should be created */
			it->second.second = make_shared
				<mup::Value>(new_value);
			mup::Variable variable(it->second.second.get());
			parser->DefineVar(it->second.first, variable);
		}

		return parser->Eval();
	} else {
		throw runtime_error(attribute + " not found in " 
				+ label + " formula");
	}
}

ostream& operator<<(ostream& dest, const Formula &value) {
	stringstream log;
	log << " Label: " << value.label << " Formula: " << value.formula;
	for (auto a : value.attributes) {
		log << " Attribute: " << a.first 
			<< " Sanitized: " << a.second.first;
	       	if (a.second.second) {
			log << " Value: " << *a.second.second.get();
		} else {
			log << " No value";
		}
	}
	dest << log.str();

	return dest;
}
