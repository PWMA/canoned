/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "convert.h"

using namespace std;

shared_ptr<CalculatingDataMessage> convert(const string &attribute, 
		Tango::DeviceAttribute &da) {
	string error;

	long data_format = da.get_data_format();
	if(data_format != Tango::SCALAR) {
		throw runtime_error(attribute + ": Only Tango::SCALAR"
				" type is supported ("
				+ to_string(data_format) + ")");
	}
#if 0
	Tango::TimeVal &t = da.get_date();
	time_t time = t.tv_sec + ((double)t.tv_usec / 1000000)
		+ ((double)t.tv_nsec / 1000000000);
#else
	time_t intime = time(NULL);
#endif

	Tango::AttrQuality &q = da.get_quality();
	string quality;
	switch(q) {
		case Tango::ATTR_INVALID:
			quality = "INVALID";
			break;
		case Tango::ATTR_VALID:
			quality = "VALID";
			break;
		case Tango::ATTR_ALARM:
			quality = "ALARM";
			break;
		case Tango::ATTR_CHANGING:
			quality = "CHANGING";
			break;
		case Tango::ATTR_WARNING:
			quality = "WARNING";
			break;
		default:
			throw runtime_error(attribute 
					+ ": Not supported quality");
	}

	if (quality != "VALID")
		throw runtime_error(attribute + ": " + quality);

	// int dim_x = da.get_dim_x(); // 0 -> ARRAY. 1 -> SCALAR, 2 -> SPECTRUM
	long data_type = da.get_type();
	switch( data_type ) {
		case Tango::DEV_LONG: {
			Tango::DevLong v;
			da >> v;
			return make_shared<IntData>(attribute, intime, v);
		} break;
		case Tango::DEV_FLOAT: {
			Tango::DevFloat v;
			da >> v;
			return make_shared<DoubleData>(attribute, intime, v);
		} break;
		case Tango::DEV_DOUBLE: {
			Tango::DevDouble v;
			da >> v;
			return make_shared<DoubleData>(attribute, intime, v);
		} break;
		case Tango::DEV_LONG64:
//				da >> TangoDataTypes.dl64;
//				return make_shared<IntData>(
//						attribute, TangoDataTypes.dl64);
		case Tango::DEV_ULONG:
//				da >> TangoDataTypes.dul;
//				return make_shared<IntData>(
//						attribute, TangoDataTypes.dul);
		case Tango::DEV_ULONG64:
//				da >> TangoDataTypes.dul64;
//				return make_shared<IntData>(
//						attribute, TangoDataTypes.dul64);
		case Tango::DEV_SHORT:
//				da >> TangoDataTypes.ds;
//				return make_shared<IntData>(
//						attribute, TangoDataTypes.ds);
		case Tango::DEV_USHORT:
//				da >> TangoDataTypes.dus;
//				return make_shared<IntData>(
//						attribute, TangoDataTypes.dus);
		case Tango::DEV_UCHAR:
//				da >> TangoDataTypes.duc;
//				return make_shared<IntData>(
//						attribute, TangoDataTypes.duc);
		case Tango::DEV_BOOLEAN:
//				da >> TangoDataTypes.db;
//				return make_shared<BoolData>(
//						attribute, TangoDataTypes.db);
		case Tango::DEV_VOID:
		case Tango::DEV_STATE:
		case Tango::DEV_STRING:
		case Tango::CONST_DEV_STRING:
		case Tango::DEVVAR_LONGARRAY:
		case Tango::DEVVAR_LONG64ARRAY:
		case Tango::DEVVAR_ULONGARRAY:
		case Tango::DEVVAR_ULONG64ARRAY:
		case Tango::DEVVAR_SHORTARRAY:
		case Tango::DEVVAR_USHORTARRAY:
		case Tango::DEVVAR_FLOATARRAY:
		case Tango::DEVVAR_DOUBLEARRAY:
		case Tango::DEVVAR_CHARARRAY:
		case Tango::DEVVAR_STRINGARRAY:
		case Tango::DEVVAR_LONGSTRINGARRAY:
		case Tango::DEVVAR_DOUBLESTRINGARRAY:
		case Tango::DEV_ENCODED:
		case Tango::DEV_ENUM:
		case Tango::DEV_PIPE_BLOB:
		case Tango::DEVVAR_BOOLEANARRAY:
		case Tango::DEVVAR_STATEARRAY:
		default:
					throw runtime_error(attribute 
							+ ": Unsupported Tango type ("
							+ to_string(data_type) + ")");
	}
}
