/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "subscription.h"

using namespace std;

Subscription::Subscription(const string &att_name_,
		shared_ptr<Tango::DeviceProxy> dp_,
		Tango::CallBack *cb_)
	: att_name(att_name_), dp(dp_), cb(cb_), event_id(0) {
	try {
		if (dp)
			event_id = dp->subscribe_event(att_name, 
					Tango::CHANGE_EVENT, cb);
	} catch(Tango::DevFailed &e) {
#ifndef NDEBUG
		clog << __PRETTY_FUNCTION__ 
			<< e.errors[0].desc << endl;
#endif
	}
}

Subscription::~Subscription() {
	try {
		if (dp && event_id)
			dp->unsubscribe_event(event_id);
	} catch(Tango::DevFailed &e) {
#ifndef NDEBUG
		clog << __PRETTY_FUNCTION__
			<< e.errors[0].desc << endl;
#endif
	}
}

bool Subscription::subscribed() noexcept {
	return event_id;
}
	
void Subscription::update(int event_id_) noexcept {
	event_id = event_id_;
}

ostream& operator<<(ostream& dest, const Subscription &value) {
	dest << "Attr_name: " << value.att_name 
		<< " DeviceProxy: " << value.dp 
		<< " EventID: " << value.event_id;
	return dest;
}
