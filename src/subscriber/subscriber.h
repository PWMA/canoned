/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <vector>
#include "../subscriberbase.h"

class Subscriber : public SubscriberBase {
public:
	Subscriber(std::shared_ptr<Channel<SubscriptionMessage>> input,
			std::vector<std::shared_ptr<Channel<CalculatingMessage>>> 
			&calculators,
			std::shared_ptr<Channel<NotificationMessage>> notifier);
	~Subscriber();
	
	void setup() noexcept;
	SubscriberBase::Result handle(std::shared_ptr<SubscribeAttributes> mesg) noexcept;
	SubscriberBase::Result handle(std::shared_ptr<UnsubscribeAttributes> mesg) noexcept;
	SubscriberBase::Result handle(std::shared_ptr<UpdateAttribute> mesg) noexcept;
	void close() noexcept;
#ifdef TEST
public:
#else
private:
#endif
	class impl;
	std::unique_ptr<impl> pimpl;
};
