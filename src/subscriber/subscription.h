/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <memory>
#include <tango.h>

class Subscription {
public:
	Subscription(const std::string &att_name, 
			std::shared_ptr<Tango::DeviceProxy> dp,
			Tango::CallBack *cb);
	~Subscription();

	bool subscribed() noexcept;
	void update(int event_id) noexcept;
private:
	const std::string att_name;
	std::shared_ptr<Tango::DeviceProxy> dp;
	Tango::CallBack *cb;
	int event_id;
	
	friend std::ostream& operator<<(std::ostream& dest, const Subscription &value);
};

std::ostream& operator<<(std::ostream& dest, const Subscription &value);
