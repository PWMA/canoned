/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>
#include <memory>
#include <unordered_map>
#include <iostream>
#include <thread>
#include <tango.h>
#include "../cache.h"
#include "convert.h"
#include "subscription.h"
#include "subscriber.h"

using namespace std;

void split(const string &attribute, string &dev_fqdn, 
		string &att_name) noexcept {
	size_t end = attribute.rfind("/");
	att_name = attribute.substr(end+1, attribute.size());
	dev_fqdn = attribute.substr(8, end-8);
}


/* Messages exchanged between Subscriber and Poller */
struct PollingMessage : Message {
	const string attribute;
	PollingMessage(const MessageType type_, 
			const string &attribute_)
		: Message(type_), attribute(attribute_) {}
	virtual ~PollingMessage() {}
};

struct PollingAttribute : PollingMessage {
	shared_ptr<Tango::DeviceProxy> dp;
	PollingAttribute(const string &attribute_,
			shared_ptr<Tango::DeviceProxy> dp_)
		: PollingMessage(SUBSCRIBE, attribute_), dp(dp_) {}
};

struct UnpollingAttribute : PollingMessage {
	UnpollingAttribute(const string &attribute_)
		: PollingMessage(UNSUBSCRIBE, attribute_) {}
};

struct Poll {
	string att_name, dev_fqdn;
	shared_ptr<Tango::DeviceProxy> dp;
	Poll(const string &att_name_,
			const string &dev_fqdn_,
			shared_ptr<Tango::DeviceProxy> dp_)
		: att_name(att_name_), dev_fqdn(dev_fqdn_), dp(dp_) {}
};

struct Subscriber::impl : public Tango::CallBack {
	shared_ptr<Channel<PollingMessage>> polling_channel;

	unordered_map<string /* attribute */,
		shared_ptr<Subscription>> attributes_subscribed;
	unordered_map<string /* attribute */,
		shared_ptr<Poll>> attributes_polled;

	Cache<Tango::DeviceProxy> proxies;

	thread poll_thread;
	Subscriber *suber;
	unsigned int ncalculators;
		
	shared_ptr<Channel<NotificationMessage>> notifier;

	impl(Subscriber *suberm, unsigned int ncalculators,
			shared_ptr<Channel<NotificationMessage>> notifier);

	void print_subscribed_structures() noexcept;
	void print_polled_structures() noexcept;
	void push_event(Tango::EventData *ev) noexcept;

	void handle(shared_ptr<PollingAttribute> mesg) noexcept;
	void handle(shared_ptr<UnpollingAttribute> mesg) noexcept;
	void handle(shared_ptr<PollingMessage> mesg) noexcept;

	void poll() noexcept;
};

Subscriber::Subscriber(shared_ptr<Channel<SubscriptionMessage>> input_,
		vector<shared_ptr<Channel<CalculatingMessage>>> &calculators_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: SubscriberBase(input_, calculators_, notifier_)
	  , pimpl{make_unique<impl>(this, calculators_.size(), notifier_)} {}

Subscriber::~Subscriber() {}
	
Subscriber::impl::impl(Subscriber *suber_, unsigned int ncalculators_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: suber(suber_), ncalculators(ncalculators_), notifier(notifier_) {
		polling_channel = make_shared<Channel<PollingMessage>>();
}

void Subscriber::impl::print_subscribed_structures() noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__
		<< " DeviceProxy Cache: " << proxies << endl;

	clog << __PRETTY_FUNCTION__;
	for (auto a : attributes_subscribed) {
		clog << " " << *a.second.get()
			<< endl;
	}
#endif
}

void Subscriber::impl::print_polled_structures() noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__;
	for (auto p : attributes_polled) {
		clog << " Attr_name: " << p.first
			<< " DeviceProxy: " << p.second->dp
			<< endl;
	}
#endif
}

void Subscriber::impl::push_event(Tango::EventData * ev) noexcept {
	/* New data from Tango so convert it to the canoned message
	 * format and forward to calculators to compute against
	 * relevant formulas */
	string error;
	try {
		if (! ev->err) {
#ifndef NDEBUG
			clog << __PRETTY_FUNCTION__ << "  " << ev->attr_name
				<< " " << *ev->attr_value << endl;
#endif
			shared_ptr<CalculatingDataMessage> value = 
				convert(ev->attr_name, *ev->attr_value);
			SubscriberBase::Result result;
			for (unsigned int i=0; i<ncalculators;++i) {
				result.cmesgs.push_back(value);
			}
			suber->pass(result);
			return;
		} else {
			error = string(ev->errors[0].desc);
		}
	} catch(exception &e) {
		error = e.what();
	} catch (...) {
		error = "Unknown error";
	}

#ifndef NDEBUG
	// FIXME: Heartbet send false error due a bug
	cerr << __PRETTY_FUNCTION__ << " " << ev->attr_name
		<< " " << error << endl;
#if 0
	notifier->write(make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, ev->attr_name + ": " + error));
#endif
#endif
}

SubscriberBase::Result Subscriber::handle(shared_ptr<UpdateAttribute> mesg) noexcept {
	/* Records that Poller has just successfully subscribed 
	 * that attributes */
	SubscriberBase::Result result;
	string attribute = *mesg->attributes.begin();
	auto it = pimpl->attributes_subscribed.find(attribute);
	if (it != pimpl->attributes_subscribed.end()) {
		it->second->update(mesg->event_id);
	} else {
		result.nmesg = make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, attribute + ": Logic error");
	}

	pimpl->print_subscribed_structures();

	return result;
}

SubscriberBase::Result Subscriber::handle(shared_ptr<SubscribeAttributes> mesg) noexcept {
#ifndef NDEBUG
	stringstream log;
	log << __PRETTY_FUNCTION__ << " subscribe";
	for (auto attribute : mesg->attributes) {
		log << " " << attribute;
	}
	clog << log.str() << endl;
#endif

	SubscriberBase::Result result;
	for (auto attribute : mesg->attributes) {
		string dev_fqdn, att_name;
		split(attribute, dev_fqdn, att_name);

		/* Retrieve the relevant DeviceProxy */
		shared_ptr<Tango::DeviceProxy> dp;
		try {
			dp = pimpl->proxies.request(dev_fqdn);
		} catch(Tango::DevFailed &e) {
#ifndef NDEBUG
			clog << __PRETTY_FUNCTION__ 
				<< e.errors[0].desc << endl;
#endif
		}

		/* Record into attributes_subscribed data structure */
		auto it = pimpl->attributes_subscribed.emplace(attribute, 
				make_shared<Subscription>
				(att_name, dp, pimpl.get()));

		if (! it.second)
			result.nmesg = make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, attribute + ": Logic error");

		if (! it.first->second->subscribed()) {
			/* If it can't subscribed to Tango so messaging 
			 * to Poller provider to poll it instead */
			pimpl->polling_channel->write(make_shared
					<PollingAttribute>(attribute, dp));
		}
	}

	pimpl->print_subscribed_structures();

	return result;
}

SubscriberBase::Result Subscriber::handle(shared_ptr<UnsubscribeAttributes> mesg) noexcept {
#ifndef NDEBUG
	stringstream log;
	log << __PRETTY_FUNCTION__ << " unsubscribe";
	for (auto attribute : mesg->attributes) {
		log << " " << attribute;
	}
	clog << log.str() << endl;
#endif

	SubscriberBase::Result result;
	for (auto attribute : mesg->attributes) {
		auto it = pimpl->attributes_subscribed.find(attribute);
		if (it != pimpl->attributes_subscribed.end()) {
			if (! it->second->subscribed()) {
				/* Subscribed attributes isn't polled by Tango so 
				 * polling should be made by canoned itself send
				 * a message to Poller provider to stop it */
				pimpl->polling_channel->write(make_shared
						<UnpollingAttribute>(attribute));
			}

			pimpl->attributes_subscribed.erase(it);
		} else {
			result.nmesg = make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, attribute + ": Logic error");
		}
	}

	pimpl->print_subscribed_structures();

	return result;
}

void Subscriber::impl::handle(shared_ptr<PollingAttribute> mesg) noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " add " << mesg->attribute
		<< " to the list of poller attributes" << endl;
#endif

	string dev_fqdn, att_name;
	split(mesg->attribute, dev_fqdn, att_name);

	auto it = attributes_polled.emplace(mesg->attribute, 
			make_shared<Poll>(att_name, 
				dev_fqdn, mesg->dp));

	if (! it.second)
		notifier->write(make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, mesg->attribute 
				+ ": Logic error"));

	/* Make first reading */
	if (mesg->dp) {
		string error;
		try {
			/* Try to read attribute from Tango */
			Tango::DeviceAttribute da;
			da = mesg->dp->read_attribute(att_name);

			/* Convert to canoned message format */	
			shared_ptr<CalculatingDataMessage> value = 
				convert(mesg->attribute, da);
			SubscriberBase::Result result;
			for (unsigned int i=0; i<ncalculators;++i) {
				result.cmesgs.push_back(value);
			}
			suber->pass(result);
		} catch(Tango::DevFailed &e) {
			error = string(e.errors[0].desc);
		} catch(exception &e) {
			error = e.what();
		} catch(...) {
			error = "Unknown error";
		}
#ifndef NDEBUG
		if (! error.empty()) {
			clog << __PRETTY_FUNCTION__ << " " 
				<< error << endl;
		}
#endif
	}

	print_polled_structures();
}

void Subscriber::impl::handle(shared_ptr<UnpollingAttribute> mesg) noexcept {
#ifndef NDEBUG
	clog << __PRETTY_FUNCTION__ << " remove " << mesg->attribute
		<< " from the list of poller attributes" << endl;
#endif

	size_t removed = attributes_polled.erase(mesg->attribute);

	if (! removed)
		notifier->write(make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, mesg->attribute 
				+ ": Logic error"));

	print_polled_structures();
}

void Subscriber::impl::handle(shared_ptr<PollingMessage> mesg) noexcept {
	switch (mesg->type) {
		case Message::SUBSCRIBE:
			handle(dynamic_pointer_cast
					<PollingAttribute>(mesg));
			break;
		case Message::UNSUBSCRIBE:
			handle(dynamic_pointer_cast
					<UnpollingAttribute>(mesg));
			break;
		default:
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__,
					 "Message not handled:"
					+ to_string(mesg->type))); 
			return;
	}
}

void Subscriber::impl::poll() noexcept {
	pthread_setname_np(pthread_self(), "poller");

	time_t wait_until = time(NULL) + 1;
	
	unordered_map<string /* att_name */,
		shared_ptr<Poll>> apc;
	unordered_map<string,
		shared_ptr<Poll>>::iterator pit 
			= apc.end();

	while(true) {
		if (pit == apc.end()) {
			/* No more attributes to poll so copy
			 * the list of polled attributes
			 * and start over it again */
			apc = attributes_polled;
			pit = apc.begin();
		}

		channel_status cs;
		vector<shared_ptr<PollingMessage>> mesgs;
		/* Wait indifintely if there aren't any attribute to poll 
		 * otherwise use timeout */
		if (apc.size()) {
			cs = polling_channel->read(mesgs, wait_until);
		} else {
			cs = polling_channel->read(mesgs, 0xFFFFFFFF);
			wait_until = time(NULL);
		}
		shared_ptr<CalculatingDataMessage> value;
		SubscriberBase::Result result;
		switch (cs) {
			case TIMEOUT: {
				string attribute = "tango://" 
					+ pit->second->dev_fqdn + "/" 
					+ pit->second->att_name;
#ifndef NDEBUG
				clog << __PRETTY_FUNCTION__ << " poll " 
					<< attribute << endl;
#endif

				/* check if the DeviceProxy is still valid 
				 * otherwise re-create it */
				if (! pit->second->dp) {
					string error;
					try {
						pit->second->dp = make_shared
							<Tango::DeviceProxy>
							(pit->second->dev_fqdn);
					} catch (Tango::DevFailed &e) {
						error = string(e.errors[0].desc);
					} catch(exception &e) {
						error = e.what();
					} catch(...) {
						error = "Unknown error";
					}
					
#ifndef NDEBUG
					if (! error.empty()) {
						cerr << __PRETTY_FUNCTION__ << " " << attribute
							<< " " << error << endl;
#if 0
						notifier->write(make_shared<NotificationSystemAlarm>
								(__PRETTY_FUNCTION__, attribute 
								 + " " + error));
#endif
					}
#endif
				}

				if (pit->second->dp) {
					string error;
					try {
						/* Try to subscribe */	
						int event_id = pit->second->dp->
							subscribe_event(
								pit->second->att_name, 
								Tango::CHANGE_EVENT, this);

						/* Subscribed so remove from poller provider */
						attributes_polled.erase(attribute);
						suber->input->write(
								make_shared<UpdateAttribute>
								(attribute, event_id));
#ifndef NDEBUG
						clog << __PRETTY_FUNCTION__ << " " 
							<< attribute << " subscribed!" 
							<< endl;
#endif
					} catch(Tango::DevFailed &e) {
						error = string(e.errors[0].desc);
					} catch(exception &e) {
						error = e.what();
					} catch(...) {
						error = "Unknown error";
					}
#ifndef NDEBUG
					if (! error.empty())
						clog << __PRETTY_FUNCTION__ << " " 
							<< error << endl;
#endif
				
					error = "";
					try {
						/* Try to read attribute from Tango */
						Tango::DeviceAttribute da;
						da = pit->second->dp
							->read_attribute(pit->second->att_name);
						
						/* Convert to canoned message format */	
						value = convert(pit->first, da);
					} catch(Tango::DevFailed &e) {
						error = string(e.errors[0].desc);
					} catch(exception &e) {
						error = e.what();
					} catch(...) {
						error = "Unknown error";
					}

					if (! error.empty()) {
#ifndef NDEBUG
						cerr << __PRETTY_FUNCTION__ << " " << attribute
							<< " " << error << endl;
#if 0
						notifier->write(make_shared<NotificationSystemAlarm>
								 (__PRETTY_FUNCTION__, attribute
								+ " " + error));
#endif
#endif
					} else {
						/* Send message to all calculators */
						for (unsigned int i=0; i<ncalculators; ++i) {
							result.cmesgs.push_back(value);
						}
						suber->pass(result);
					}
				}

				/* Increase iterator and set the new timeout */
				++pit;
				wait_until += 1;
				continue;
			}
			case CLOSE:
				return;
			case DATA:
				for(auto mesg : mesgs)
					handle(mesg);
				continue;
		}
	}
}

void Subscriber::setup() noexcept {
	pimpl->poll_thread = thread(bind(
				&Subscriber::impl::poll, 
				pimpl.get()));
}

void Subscriber::close() noexcept {
	pimpl->polling_channel->close();

	if (pimpl->poll_thread.joinable())
		pimpl->poll_thread.join();
}
