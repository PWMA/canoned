/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include "plannerbase.h"

using namespace std;

PlannerBase::PlannerBase(shared_ptr<Channel<PlanningMessage>> input_,
		shared_ptr<Channel<SubscriptionMessage>> subscriber_,
		vector<shared_ptr<Channel<CalculatingMessage>>> &calculators_,
		shared_ptr<Channel<DispatchingMessage>> dispatcher_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: input(input_), subscriber(subscriber_), calculators(calculators_)
	  , dispatcher(dispatcher_), notifier(notifier_) {}

PlannerBase::~PlannerBase() {}

void PlannerBase::handle(shared_ptr<PlanningMessage> mesg) noexcept {
	switch (mesg->type) {
		case Message::SUBSCRIBE:
			pass(handle(dynamic_pointer_cast
					<PlanningSubscribe>(mesg)));
			break;
		case Message::UNSUBSCRIBE:
			pass(handle(dynamic_pointer_cast
					<PlanningUnsubscribe>(mesg)));
			break;
		default:
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__,
					 "Message not handled:"
					+ to_string(mesg->type)));
			return;
	}
}

void PlannerBase::pass(Result result) noexcept {
	if (result.nmesg)
		notifier->write(result.nmesg);

	if (result.dmesg)
		dispatcher->write(result.dmesg);

	for (size_t i=0; i<result.cmesgs.size(); ++i) {
		if (result.cmesgs[i]) {
			calculators[i]->write(result.cmesgs[i]);
		}
	}

	if (result.smesg)
		subscriber->write(result.smesg);
}

void PlannerBase::setup() noexcept {}

time_t PlannerBase::prepare() noexcept {
	return 0xFFFFFFFF;
}

void PlannerBase::timeout() noexcept {
	notifier->write(make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Wrong invocation"));
}

void PlannerBase::close() noexcept {}

void PlannerBase::loop() noexcept {
	pthread_setname_np(pthread_self(), "planner");

	setup();

	while(true) {
		time_t wait_until = prepare();

		vector<shared_ptr<PlanningMessage>> mesgs;
		switch (input->read(mesgs, wait_until))
		{
			case CLOSE:
				close();
				return;
			case TIMEOUT:
				timeout();
				continue;
			case DATA:
				for (auto mesg : mesgs)
					handle(mesg);
		}
	}
}
