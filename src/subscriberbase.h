/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include "channel.h"
#include "messages.h"

class SubscriberBase {
public:
	SubscriberBase(std::shared_ptr<Channel<SubscriptionMessage>> input,
			std::vector<std::shared_ptr<Channel<CalculatingMessage>>> 
				&calculators,
			std::shared_ptr<Channel<NotificationMessage>> notifier);
	virtual ~SubscriberBase();
	
	void loop() noexcept;
protected:
	std::shared_ptr<Channel<SubscriptionMessage>> input;
	struct Result {
		std::vector<std::shared_ptr<CalculatingMessage>> cmesgs;
		std::shared_ptr<NotificationMessage> nmesg;
	};
	void pass(Result result) noexcept;

	virtual void setup() noexcept;
	virtual time_t prepare() noexcept;
	virtual void timeout() noexcept;
	virtual Result handle(std::shared_ptr<SubscribeAttributes> mesg) noexcept = 0;
	virtual Result handle(std::shared_ptr<UnsubscribeAttributes> mesg) noexcept = 0;
	virtual Result handle(std::shared_ptr<UpdateAttribute> mesg) noexcept = 0;
	virtual void close() noexcept;
private:
	std::vector<std::shared_ptr<Channel<CalculatingMessage>>> calculators;
	std::shared_ptr<Channel<NotificationMessage>> notifier;
	
	void handle(std::shared_ptr<SubscriptionMessage> mesg) noexcept;
};
