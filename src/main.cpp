/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <getopt.h>
#include <grp.h>
#include <pwd.h>
#include <csignal>
#include <cstring>
#include <fstream>
#include <iostream>
#include <thread>
#include <vector>
#include <functional>
#include <sstream>
#include "syslog.h"
#include "channel.h"
#include "messages.h"
#include "receiver/receiver.h"
#include "planner/planner.h"
#include "subscriber/subscriber.h"
#include "calculator/calculator.h"
#include "dispatcher/dispatcher.h"
#include "notifier/notifier.h"
#include "dblogger/dblogger.h"

using namespace std;

/* Write cvs tag into the objet file (necessary here at Elettra) */
const char *TagName = "$Name:  $";

/* Canoned
 *
 * Canoned is composed by Providers and Channels.
 *
 *
 * Providers
 *
 * Receiver receives users' requests, check if mandatory data are not empty
 * and convert to canoned message. It also sends directly to dispatcher 
 * a formula ack.
 *
 * Planner records users' requests avoid the duplicated ones and send message
 * to the others providers to keep update their local data structures.
 *
 * Subscriber subscribes Tango attributes (or any other pub/sub event system)
 * to obtain new value every time an attribute changed. It provides also a
 * internal poller for attributes which aren't polled by Tango.
 *
 * Calculator do the mathematical formula computation. It is heavy and frequent
 * task so more than one of this is launched.
 *
 * Dispatcher applies dispatching rules and handles delayed alarms 
 * and formula acks.
 *
 * Notifier converts from canoned message format to the suitable one for the 
 * external service used for delivery notification to user's device 
 * (Firebase Cloud Message).
 *
 * DBLogger stores important events into database (PostgreSQL)
 *
 *
 * Channels
 *
 * Are used by various canoned's parts (named Providers) to talk each other
 * to avoid latency propagation and locking chaining by queue and data duplication.
 * In the above schema channels are represented as lines.
 *
 *
 * Cannoned flow
 *
 * Canoned entry-point is Receiver and the exit-point is Notifier.
 *
 * -> Receiver -> Planner -> Subscriber -> Calculator0 -> Dispatcher -> Notifier ->
 *          |          |      |    /\   |                /\       |     |
 *          |          |      |     |   |                 |       |     |
 *          |          |      |     |   |-> Calculator1 ->|      \/     \/
 *          |          |     \/     |   |                 |       DbLogger
 *          |          |    Poller->|   |-> Calculator2 ->|
 *          |          |                |                 |
 *          |          |                |-> ........... ->|
 *          |          |                |                 |
 *          |          |                |-> CalculatorN ->|
 *          |          |                                  |
 *         \/__________\/_________________________________|
 */

shared_ptr<Channel<StoreMessage>> dblogging_channel;
shared_ptr<Channel<NotificationMessage>> notification_channel;
shared_ptr<Channel<DispatchingMessage>> dispatching_channel;
vector<shared_ptr<Channel<CalculatingMessage>>> calculating_channels;
shared_ptr<Channel<SubscriptionMessage>> subscribing_channel;
shared_ptr<Channel<PlanningMessage>> planning_channel;
shared_ptr<Channel<Empty>> receiving_channel;

sig_atomic_t term_received;
static void sighup_handler(int signum)
{
	stringstream log;
	switch(signum) {
		case SIGHUP:
			clog << __PRETTY_FUNCTION__ 
				<< " SIGHUP signal received" 
				<< endl;
			/* Nothing to do yet (theoritically it should reads 
			 * the configuration file again) */
			break;
		case SIGTERM:
			clog << __PRETTY_FUNCTION__ 
				<< " SIGTERM/SIGINT signal received" 
				<< endl;
			[[gnu::fallthrough]];
		case SIGINT:
			/* Flag used to stop the main loop */
			term_received = 1;
			break;
		case SIGUSR1:
			/* Print out the numbers of messages in all channels 
			 * not served yet */
			log << __PRETTY_FUNCTION__
				<< " PL: " << planning_channel->size()
				<< " SU: " << subscribing_channel->size();
			for (auto cc : calculating_channels) {
				log << " C :" << cc->size();
			}
			clog << log.str() 
				<< " DI: " << dispatching_channel->size()
				<< " NO: " << notification_channel->size()
				<< endl;
			break;
		default:
			cerr << __PRETTY_FUNCTION__ << " Unhandled signal (" 
				<< signum << ")" << endl;
	}
}

int main(int argc , char *argv[])
{
	struct sigaction sa_restarted;
	const char *user = "nobody";
	const char *group = "nogroup";
	string conninfo, serverkey, smtpserver, adminemail;
	vector<char*> options = {argv[0]};
	bool foreground = false;

	clog << __PRETTY_FUNCTION__ << " Starting Canoned" << endl;

	/* Read option parameters from configuration file */
	ifstream ifs ("/etc/canoned.conf", ifstream::in);
	char *line = new char[1000];
	line[0] = line[1] = '-';
	ifs.getline(line+2, 100-2);
	while (ifs.good()) {
		options.push_back(line);
		line = new char[1000];
		line[0] = line[1] = '-';
		ifs.getline(line+2, 1000-2);
	}
	
	/* Read option parameters from command line */
	for(int i=1; i<argc; ++i) {
		options.push_back(argv[i]);
	}

	/* Parse option parameters (the last wins) */
	while (1)
	{
		static struct option long_options[] =
		{
			/* User and group to drop into if it is launched
			 * as root */
			{"user",		required_argument,	0, 1},
			{"group",		required_argument,	0, 2},
			/* Postgres connection parameters */
			{"pghost",		required_argument,	0, 3},
			{"pgdbname",		required_argument,	0, 4},
			{"pguser",		required_argument,	0, 5},
			{"pgpassword",		required_argument,	0, 6},
			/* Avoid canoned to go into background */
			{"foreground",		no_argument,		0, 7},
			/* FCM server secret key */
			{"fcmserverkey",	required_argument,	0, 8},
			/* FCM token to which send all strange things happens */
			{"smtpserver",		optional_argument,	0, 9},
			{"adminemail",		optional_argument,	0, 10},
			{0, 0, 0, 0}
		};

		/* getopt_long stores the option index here. */
		int option_index = 0;

		int c = getopt_long(options.size(), &options[0], "",
				long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c)
		{
			case 1:
				user = optarg;
				break;
			case 2:
				group = optarg;
				break;
			case 3:
				conninfo += string("host=") + optarg + " ";;
				break;
			case 4:
				conninfo += string("dbname=") + optarg + " ";;
				break;
			case 5:
				conninfo += string("user=") + optarg + " ";;
				break;
			case 6:
				conninfo += string("password=") + optarg + " ";;
				break;
			case 7:
				foreground = true;
				break;
			case 8:
				serverkey = optarg;
				break;
			case 9:
				smtpserver = optarg;
				break;
			case 10:
				adminemail = optarg;
				break;
			case '?':
				/* getopt_long already printed an error message. */
				return EXIT_FAILURE;
			default:
				cerr << __PRETTY_FUNCTION__ 
					<< " Unhandled option parameter: " 
					<< string(optarg) << endl;
				return EXIT_FAILURE;
		}
	}

	/* Free vector of option parameters */	
	for (size_t i=1; i < (options.size() - (argc-1)); ++i) {
		delete options[i];
	}

#if 0
	/* Print any remaining command line arguments (not options). */
	if (optind < argc)
	{
		printf ("non-option ARGV-elements: ");
		while (optind < argc)
			printf ("%s ", argv[optind++]);
		putchar ('\n');
	}

#endif

	if (getuid() == 0) {
		/* process is running as root, drop privileges */
		struct group *grp = getgrnam(group);
		if (grp == 0) {
			cerr << __PRETTY_FUNCTION__ 
				<< " Unable to retrieve the gid for group "
				<< user << ": " << strerror(errno) << endl;
			return EXIT_FAILURE;
		}

		struct passwd *usr = getpwnam(user);
		if (usr == 0) {
			cerr << __PRETTY_FUNCTION__ 
				<< " Unable to retrieve the uid for group "
				<< user << ": " << strerror(errno) << endl;
			return EXIT_FAILURE;
		}

		if (setgid(grp->gr_gid) != 0) {
			cerr << __PRETTY_FUNCTION__ 
				<< " Unable to drop group privilegies: "
				<< strerror(errno) << endl;
			return EXIT_FAILURE;
		}

		if (setuid(usr->pw_uid) != 0) {
			cerr << __PRETTY_FUNCTION__ 
				<< " Unable to user group privilegies: "
				<< strerror(errno) << endl;
			return EXIT_FAILURE;
		}
	}

	/* Setup signals handler */
	memset(&sa_restarted, 0, sizeof(struct sigaction));
	sa_restarted.sa_handler = sighup_handler;
	sa_restarted.sa_flags = SA_RESTART;
	sigemptyset(&sa_restarted.sa_mask);
	if (sigaction(SIGHUP, &sa_restarted, 0) == -1 ||
			sigaction(SIGTERM, &sa_restarted, 0) == -1 ||
			sigaction(SIGUSR1, &sa_restarted, 0) == -1) {
		cerr << __PRETTY_FUNCTION__ << " Unable to configure signals: " 
			<< strerror(errno) << endl;
		return EXIT_FAILURE;
	}

	if (!foreground) {
		/* Daemonize */	
		if (daemon(0,0) == -1) {
			cerr << __PRETTY_FUNCTION__ << " Unable to go in background: " 
				<< strerror(errno) << endl;
			return EXIT_FAILURE;
		}

		/* Flush and redirect clog and cerr to syslog */
		clog << flush;
		cerr << flush;
		clog.rdbuf(new Syslog(SYSLOG_DEBUG));
		cerr.rdbuf(new Syslog(SYSLOG_ERROR));
	} else {
		/* Interactive session: add SIGINT to signals handler */
		if (sigaction(SIGINT, &sa_restarted, 0) == -1) {
			cerr << __PRETTY_FUNCTION__ << " Unable to configure signals: " 
				<< strerror(errno) << endl;
			return EXIT_FAILURE;
		}
	}

	/* Create channels */
	dblogging_channel = make_shared<Channel<StoreMessage>>();
	notification_channel = make_shared<Channel<NotificationMessage>>();
	dispatching_channel = make_shared<Channel<DispatchingMessage>>();
	unsigned int hardware_concurrency = thread::hardware_concurrency();
	for (unsigned int i=0; i<hardware_concurrency; ++i) {
		calculating_channels.push_back(make_shared<Channel
				<CalculatingMessage>>());
	}
	subscribing_channel = make_shared<Channel<SubscriptionMessage>>();
	planning_channel = make_shared<Channel<PlanningMessage>>();
	receiving_channel = make_shared<Channel<Empty>>();

	/* Create providers */
	unique_ptr<DbLoggerBase> dblogger =
		make_unique<DbLogger>(conninfo, dblogging_channel,
				notification_channel);

	unique_ptr<NotifierBase> notifier =
		make_unique<Notifier>(smtpserver, adminemail, serverkey,
				notification_channel, dblogging_channel, foreground);

	unique_ptr<DispatcherBase> dispatcher =
		make_unique<Dispatcher>(dispatching_channel, 
				notification_channel, dblogging_channel);

	vector<shared_ptr<CalculatorBase>> calculators;
	for (unsigned int i=0; i<hardware_concurrency; ++i) {
		calculators.push_back(make_shared<Calculator>(i,
					calculating_channels[i],
					dispatching_channel,
					notification_channel));
	}

	unique_ptr<SubscriberBase> subscriber =
		make_unique<Subscriber>(subscribing_channel, 
				calculating_channels, notification_channel);

	unique_ptr<PlannerBase> planner =
		make_unique<Planner>(planning_channel, 
				subscribing_channel, calculating_channels,
				dispatching_channel, notification_channel);

	unique_ptr<ReceiverBase> receiver =
		make_unique<Receiver>(conninfo, 
				receiving_channel, planning_channel, 
				dispatching_channel, notification_channel,
				dblogging_channel);

	/* Start threads */
	vector<thread> threads;

	threads.emplace_back(bind(&DbLoggerBase::loop, dblogger.get()));
	threads.emplace_back(bind(&NotifierBase::loop, notifier.get()));
	threads.emplace_back(bind(&DispatcherBase::loop, dispatcher.get()));
	
	for (unsigned int i=0; i<hardware_concurrency; ++i) {
		threads.emplace_back(bind(&CalculatorBase::loop, 
					calculators[i].get()));
	}

	threads.emplace_back(bind(&SubscriberBase::loop, subscriber.get()));
	threads.emplace_back(bind(&PlannerBase::loop, planner.get()));
	threads.emplace_back(bind(&ReceiverBase::loop, receiver.get()));

	clog << __PRETTY_FUNCTION__ << " " << "Canoned started" << endl;
#ifdef NDEBUG
	notification_channel->write(make_shared
			<NotificationSystemAlarm>(__PRETTY_FUNCTION__,
				"Canoned started"));
#endif

	/* Wait for signal */
	while(! term_received) {
		pause();
	}

	clog << __PRETTY_FUNCTION__ << " Stopping Canoned" << endl;

	/* Close queues */
	receiving_channel->close();
	planning_channel->close();
	subscribing_channel->close();
	for (auto cc : calculating_channels) {
		cc->close();
	}
	dispatching_channel->close();
	notification_channel->close();
	dblogging_channel->close();
	
	/* Stop threads */
	for(size_t i=0; i<threads.size(); ++i)
		threads[i].join();

	clog << __PRETTY_FUNCTION__ << " Canoned stopped" << endl;

	return EXIT_SUCCESS;
}

