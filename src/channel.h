/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <chrono>

enum channel_status { CLOSE, TIMEOUT, DATA };

template<typename T> class Channel
{
public:
	Channel() : open(true) {}

	size_t size() noexcept {
		std::lock_guard<std::mutex> lock(mutex_);
		return queue_.size();
	}

	void close() noexcept {
		mutex_.lock();
		open = false;
		mutex_.unlock();

		condition.notify_all();
	}

	channel_status read(std::vector<std::shared_ptr<T>> &data, 
			time_t wait_until) noexcept {
		std::cv_status wait_status;
		std::chrono::system_clock::time_point time_point = 
			std::chrono::system_clock::from_time_t(wait_until);

		std::unique_lock<std::mutex> lock(mutex_);
		while (true) {
			size_t qs = queue_.size();
			if (! qs) {
				if (! open)
					return CLOSE;
			
				wait_status = condition.wait_until(lock,
						time_point);
				switch(wait_status) {
					case std::cv_status::timeout:
						return TIMEOUT;
					case std::cv_status::no_timeout:
						qs = queue_.size();
				}
			}

			for(size_t i=0; i<qs; ++i) {
				data.push_back(queue_.front());
				queue_.pop();
			}

			if (! open)
				return CLOSE;

			if (qs)
				return DATA;
		}
	}

	void write(const std::shared_ptr<T> data) noexcept {
		mutex_.lock();
		queue_.push(data);
		mutex_.unlock();
			
		condition.notify_one();
	}
private:
	std::queue<std::shared_ptr<T>> queue_;
	std::mutex mutex_;
	std::condition_variable condition;
	bool open;
};
