/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <unordered_map>
#include <string>
#include <memory>

template<typename T> class Cache {
private:
	std::unordered_map<std::string, std::weak_ptr<T>> cache;
public:
	std::shared_ptr<T> request(const std::string &key) {
		std::string key_ = key;
		std::shared_ptr<T> sptr;
		auto it = cache.find(key);
		if (it != cache.end()) {
			if (it->second.expired()) {
				sptr = std::make_shared<T>(key_);
				it->second = sptr;
			} else {
				sptr = std::shared_ptr<T>(it->second);
			}
		} else {
			sptr = std::make_shared<T>(key_);
			cache.emplace(key_, sptr);
		}

		return sptr;
	}

	template<typename C>
		friend std::ostream& operator<<(std::ostream& dest, 
			const Cache<C> &value);
};

template<typename C> std::ostream& operator<<(std::ostream& dest, const Cache<C> &value ) {
	std::stringstream log;
	for (auto c : value.cache) {
		log << c.first << " "
			<< c.second.use_count() << " ";
	}
	dest << log.str();
	return dest;
}
