/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once 

#include <string>
#include <set>
#include <vector>
#include <utility>

struct Empty {};

struct Message {
	const enum MessageType { SUBSCRIBE, UNSUBSCRIBE,
		ADDCONFIG, REMOVECONFIG, SENDUSERALARM, SENDSYSTEMALARM,
		ACK, UPDATE, DATA } type;
	Message(const MessageType type_) : type(type_) {}
	virtual ~Message() {}
};

struct SubscriptionMessage : Message {
	const std::set<std::string> attributes;
	SubscriptionMessage(const MessageType type_, 
			const std::set<std::string> &attributes_)
		: Message(type_), attributes(attributes_) {}
	virtual ~SubscriptionMessage() {}
};

struct SubscribeAttributes : SubscriptionMessage {
	SubscribeAttributes(const std::set<std::string> &attributes_)
		: SubscriptionMessage(SUBSCRIBE, attributes_) {}
};

struct UnsubscribeAttributes : SubscriptionMessage {
	UnsubscribeAttributes(const std::set<std::string> &attributes_)
		: SubscriptionMessage(UNSUBSCRIBE, attributes_) {}
};

struct UpdateAttribute : SubscriptionMessage {
	const int event_id;
	UpdateAttribute(const std::string &attribute_, const int event_id_)
		: SubscriptionMessage(UPDATE, {attribute_}), 
		event_id(event_id_) {}
};

struct PlanningMessage : Message {
	const std::string label, token;	
	PlanningMessage(const MessageType type_, const std::string &label_, 
			const std::string &token_)
		: Message(type_), label(label_), token(token_) {}
	virtual ~PlanningMessage() {}
};

struct PlanningSubscribe : PlanningMessage {
	const std::string formula, note;
	const long delay;
	const std::string username;
	const bool ack;
	const bool firstconf;
	PlanningSubscribe(const std::string &label_, const std::string &token_,
			const std::string &formula_, const std::string &note_,
			const long delay_, const std::string &username_,
			const bool ack_, const bool firstconf_)
		: PlanningMessage(SUBSCRIBE, label_, token_), 
			formula(formula_), note(note_), delay(delay_),
				username(username_), ack(ack_), firstconf(firstconf_) {}
};

struct PlanningUnsubscribe : PlanningMessage {
	PlanningUnsubscribe(const std::string &label_, const std::string &token_)
		: PlanningMessage(UNSUBSCRIBE, label_, token_) {}
};

struct NotificationMessage : Message {
	const std::string body;
	NotificationMessage(MessageType type_, const std::string &body_)
		: Message(type_), body(body_) {}
	virtual ~NotificationMessage() {}
};

struct NotificationUserAlarm : NotificationMessage {
	const std::string label;
	const time_t time;
	const std::vector<std::pair<std::string,std::string>> tokens;
	NotificationUserAlarm(const std::string &label_,
			const time_t time_,
			const std::vector<std::pair<std::string,std::string>> &tokens_,
			const std::string &body_)
		: NotificationMessage(SENDUSERALARM, body_), 
			label(label_), time(time_), tokens(tokens_) {}
};

struct NotificationSystemAlarm : NotificationMessage {
	const std::string title;
	NotificationSystemAlarm(const std::string &title_,
			const std::string &body_)
		: NotificationMessage(SENDSYSTEMALARM, body_),
       			title(title_) {}
};

struct DispatchingMessage : Message {
	const std::string label;
	DispatchingMessage(MessageType type_, const std::string &label_) 
		: Message(type_), label(label_) {}
	virtual ~DispatchingMessage() {}
};

struct DispatchingAck : DispatchingMessage {
	const std::string token;
	DispatchingAck(const std::string &label_, const std::string &token_)
		: DispatchingMessage(ACK, label_), token(token_) {}
};


struct DispatchingAddConfig : DispatchingMessage {
	const std::string note, token;
	const long delay;
	const std::string username;
	const bool ack;
	const bool firstconf;
	DispatchingAddConfig(const std::string &label_, const std::string &note_,
			const std::string &token_, const long delay_,
			const std::string &username_, const bool ack_, const bool firstconf_)
		: DispatchingMessage(ADDCONFIG, label_), note(note_),
			token(token_), delay(delay_),
			username(username_), ack(ack_), firstconf(firstconf_) {}
};

struct DispatchingRemoveConfig : DispatchingMessage {
	const std::string token;
	DispatchingRemoveConfig(const std::string &label_, const std::string &token_)
		: DispatchingMessage(REMOVECONFIG, label_), token(token_) {}
};

struct DispatchingUpdate : DispatchingMessage {
	const time_t time;
	const bool result;
	DispatchingUpdate(const std::string &label_,
			const time_t time_, const bool result_)
		: DispatchingMessage(UPDATE, label_),
			time(time_), result(result_) {}
};

struct CalculatingMessage : Message {
	CalculatingMessage(MessageType type_) : Message(type_) {}
	virtual ~CalculatingMessage() {}
};

struct CalculatingAddConfig : CalculatingMessage {
	const std::string label, formula;
	const std::set<std::string> attributes;
	CalculatingAddConfig(const std::string &label_, 
			const std::string &formula_, 
			const std::set<std::string> &attributes_)
		: CalculatingMessage(ADDCONFIG), label(label_), 
			formula(formula_), attributes(attributes_) {}
};

struct CalculatingRemoveConfig : CalculatingMessage {
	const std::string label;
	const std::set<std::string> attributes;
	CalculatingRemoveConfig(const std::string &label_, 
			const std::set<std::string> &attributes_)
		: CalculatingMessage(REMOVECONFIG), label(label_), 
			attributes(attributes_) {}
};

struct CalculatingDataMessage : CalculatingMessage {
	const enum DataType { BOOLDATA, DOUBLEDATA,
		STRINGDATA, INTDATA } type;
	const std::string attribute;
	const time_t time;
	CalculatingDataMessage(const DataType type_, 
			const std::string &attribute_, const time_t time_)
		: CalculatingMessage(DATA), type(type_), 
			attribute(attribute_), time(time_) {}
	virtual ~CalculatingDataMessage() {}
};

struct BoolData : CalculatingDataMessage {
	const bool data;
	BoolData(const std::string &attribute_, const time_t time_,
			const bool data_)
		: CalculatingDataMessage(BOOLDATA, attribute_, time_),
	       		data(data_) {}
};

struct DoubleData : CalculatingDataMessage {
	const double data;
	DoubleData(const std::string &attribute_, const time_t time_,
			const double data_)
		: CalculatingDataMessage(DOUBLEDATA, attribute_, time_),
			data(data_) {}
};

struct StringData : CalculatingDataMessage {
	const std::string data;
	StringData(const std::string &attribute_, const time_t time_,
			const std::string &data_)
		: CalculatingDataMessage(STRINGDATA, attribute_, time_),
			data(data_) {}
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
struct IntData : CalculatingDataMessage {
	const __int128 data;
	IntData(const std::string &attribute_, const time_t time_,
			const __int128 &data_)
		: CalculatingDataMessage(INTDATA, attribute_, time_), 
			data(data_) {}
};
#pragma GCC diagnostic pop

struct StoreMessage : Message {
	const std::string token;
	StoreMessage(MessageType type_, const std::string &token_)
		: Message(type_), token(token_) {}
	virtual ~StoreMessage() {}
};

struct StoreAddConfig : StoreMessage {
	const int id;
	StoreAddConfig(const std::string &token_, const int id_)
		: StoreMessage(ADDCONFIG, token_), id(id_) {}
};

struct StoreAlarmMessage : StoreMessage {
	const std::string label;
	const bool flag;
	StoreAlarmMessage(MessageType type_, const std::string &label_,
			const std::string &token_, const bool flag_)
		: StoreMessage(type_, token_), label(label_), flag(flag_) {}
	virtual ~StoreAlarmMessage() {}
};

struct StoreAlarmAck : StoreAlarmMessage {
	StoreAlarmAck(const std::string &label_, const std::string &token_)
		: StoreAlarmMessage(ACK, label_, token_, true) {}
};

struct StoreAlarmAdd : StoreAlarmMessage {
	StoreAlarmAdd(const std::string &label_, const std::string &token_)
		: StoreAlarmMessage(SUBSCRIBE, label_, token_, true) {}
};

struct StoreAlarmRemove : StoreAlarmMessage {
	StoreAlarmRemove(const std::string &label_, const std::string &token_)
		: StoreAlarmMessage(UNSUBSCRIBE, label_, token_, false) {}
};

struct StoreAlarmNotify : StoreAlarmMessage {
	const time_t time;
	StoreAlarmNotify(const std::string &label_, const time_t time_,
			const std::string &token_)
		: StoreAlarmMessage(UPDATE, label_, token_, true), time(time_) {}
};
