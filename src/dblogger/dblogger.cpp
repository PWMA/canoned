/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <unordered_map>
#include <libpq-fe.h>
#include "dblogger.h"

using namespace std;


struct DbLogger::impl {
	const string conninfo;
	PGconn *conn;
	shared_ptr<Channel<NotificationMessage>> notifier;

	unordered_map<string /* token */, int /* tokenid */> ids;

	impl(const string &conninfo_,
			shared_ptr<Channel<NotificationMessage>> notifier_);
	~impl();

	void store(const string &label, const time_t time, const string &token,
			const bool ack, const bool subscribe,
			const bool notify) noexcept;
};

DbLogger::DbLogger(const string &conninfo_,
		shared_ptr<Channel<StoreMessage>> input_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: DbLoggerBase(input_, notifier_),
		pimpl{make_unique<impl>(conninfo_, notifier_)} {}

DbLogger::~DbLogger() {
}

DbLogger::impl::impl(const string &conninfo_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: conninfo(conninfo_), conn(NULL), notifier(notifier_) {}

DbLogger::impl::~impl() {}

void DbLogger::impl::store(const string &label, const time_t time,
		const string &token, const bool ack, const bool subscribe,
		const bool notify) noexcept {
	stringstream emesg;
	if (! conn) {
		conn = PQconnectdb(conninfo.c_str());
		if (PQstatus(conn) != CONNECTION_OK) {
			emesg << "Database connection failed: " << PQerrorMessage(conn);
			PQfinish(conn);
			conn = NULL;
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__, emesg.str()));
			return;
		}
	}
		
	if ((PQping(conninfo.c_str()) != PQPING_OK)) {
		emesg << "Database doesn't respond anymore";
		PQfinish(conn);
		conn = NULL;
		notifier->write(make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, emesg.str()));
		return;
	}

	auto it = ids.find(token);
	if (it == ids.end()) {
		emesg << "Token " + token + " doesn't found!";
		notifier->write(make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, emesg.str()));
		return;
	}
		
	stringstream query;
	query << "INSERT INTO alarmlog(\"label\", \"tokenid\"," 
		<< "\"date\", \"ack\", \"subscribe\", \"rtvalue\") VALUES(" 
		<< "'" << label << "', '" << it->second << "', "
		<< "to_timestamp(" << time << ")," << "'" << ack
		<< "', '" << subscribe << "', '" << notify << "')";
	PGresult *res = PQexec(conn, query.str().c_str());
	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		emesg << "Query " << query.str() << " failed due "
			<< PQerrorMessage(conn);
		PQclear(res);
		PQfinish(conn);
		conn = NULL;
		notifier->write(make_shared<NotificationSystemAlarm>
				(__PRETTY_FUNCTION__, emesg.str()));
		return;
	}
	PQclear(res);
}

void DbLogger::handle(shared_ptr<StoreAddConfig> mesg) noexcept {
	pimpl->ids.emplace(mesg->token, mesg->id);
}

void DbLogger::handle(shared_ptr<StoreAlarmAck> mesg) noexcept {
	pimpl->store(mesg->label, time(NULL), mesg->token,
			mesg->flag, true, false);
}

void DbLogger::handle(shared_ptr<StoreAlarmNotify> mesg) noexcept {
	// FIXME
#if 0
	pimpl->store(mesg->label, mesg->time, mesg->token,
			false, true, mesg->flag);
#endif
	pimpl->store(mesg->label, time(NULL), mesg->token,
			false, true, mesg->flag);
}

void DbLogger::handle(shared_ptr<StoreAlarmAdd> mesg) noexcept {
	pimpl->store(mesg->label, time(NULL), mesg->token,
			true, mesg->flag, false);
}

void DbLogger::handle(shared_ptr<StoreAlarmRemove> mesg) noexcept {
	pimpl->store(mesg->label, time(NULL), mesg->token,
			false, mesg->flag, false);
}
