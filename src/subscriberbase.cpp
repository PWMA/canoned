/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include "subscriberbase.h"

using namespace std;

SubscriberBase::SubscriberBase(shared_ptr<Channel<SubscriptionMessage>> input_,
		vector<shared_ptr<Channel<CalculatingMessage>>> &calculators_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: input(input_), calculators(calculators_), notifier(notifier_) {}

SubscriberBase::~SubscriberBase() {}

void SubscriberBase::handle(shared_ptr<SubscriptionMessage> mesg) noexcept {
	switch (mesg->type) {
		case Message::SUBSCRIBE:
			pass(handle(dynamic_pointer_cast
					<SubscribeAttributes>(mesg)));
			break;
		case Message::UNSUBSCRIBE:
			pass(handle(dynamic_pointer_cast
					<UnsubscribeAttributes>(mesg)));
			break;
		case Message::UPDATE:
			pass(handle(dynamic_pointer_cast
					<UpdateAttribute>(mesg)));
			break;
		default:
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__,
					"Message not handled:"
					+ to_string(mesg->type)));
			return;
	}
}

void SubscriberBase::pass(Result result) noexcept {
	if (result.nmesg)
		notifier->write(result.nmesg);

	for (size_t i=0; i<result.cmesgs.size(); ++i) {
		if (result.cmesgs[i])
			calculators[i]->write(result.cmesgs[i]);
	}
}

void SubscriberBase::setup() noexcept {}

time_t SubscriberBase::prepare() noexcept {
	return 0xFFFFFFFF;
}

void SubscriberBase::timeout() noexcept {
	notifier->write(make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Wrong invocation"));
}

void SubscriberBase::close() noexcept {}

void SubscriberBase::loop() noexcept {
	pthread_setname_np(pthread_self(), "subscriber");

	setup();

	while(true) {
		time_t wait_until = prepare();

		vector<shared_ptr<SubscriptionMessage>> mesgs;
		switch (input->read(mesgs, wait_until))
		{
			case TIMEOUT:
				timeout();
				continue;
			case CLOSE:
				close();
				return;
			case DATA:
				for(auto mesg : mesgs)
					handle(mesg);
		}
	}
}
