/* Copyright (C) 2018  Alessio Igor Bogani <alessio.bogani@elettra.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include "calculatorbase.h"

using namespace std;

CalculatorBase::CalculatorBase(unsigned int id_,
		shared_ptr<Channel<CalculatingMessage>> input_,
		shared_ptr<Channel<DispatchingMessage>> dispatcher_,
		shared_ptr<Channel<NotificationMessage>> notifier_)
	: id(id_), input(input_), dispatcher(dispatcher_), notifier(notifier_) {}

CalculatorBase::~CalculatorBase() {}

void CalculatorBase::handle(shared_ptr<CalculatingMessage> mesg) noexcept {
	switch (mesg->type) {
		case Message::ADDCONFIG:
			pass(handle(dynamic_pointer_cast
					<CalculatingAddConfig>(mesg)));
			break;
		case Message::REMOVECONFIG:
			pass(handle(dynamic_pointer_cast
					<CalculatingRemoveConfig>(mesg)));
			break;
		case Message::DATA:
			pass(handle(dynamic_pointer_cast
					<CalculatingDataMessage>(mesg)));
			break;
		default:
			notifier->write(make_shared<NotificationSystemAlarm>
					(__PRETTY_FUNCTION__,
					"Message not handled:"
					+ to_string(mesg->type)));
			return;
	}
}

void CalculatorBase::pass(Result result) noexcept {
	for (auto dmesg : result.dmesgs) {
		if (dmesg)
			dispatcher->write(dmesg);
	}
}

void CalculatorBase::setup() noexcept {}

time_t CalculatorBase::prepare() noexcept {
	return 0xFFFFFFFF;
}

void CalculatorBase::timeout() noexcept {
	notifier->write(make_shared<NotificationSystemAlarm>
			(__PRETTY_FUNCTION__, "Wrong invocation"));
}

void CalculatorBase::close() noexcept {}

void CalculatorBase::loop() noexcept {
	string thread_name = "calculator" + to_string(id);
	pthread_setname_np(pthread_self(), thread_name.c_str());

	setup();

	while(true) {
		time_t wait_until = prepare();

		vector<shared_ptr<CalculatingMessage>> mesgs;
		switch (input->read(mesgs, wait_until))
		{
			case CLOSE:
				close();
				return;
			case TIMEOUT:
				timeout();
				continue;
			case DATA:
				for (auto mesg : mesgs)
					handle(mesg);
		}
	}
}
